/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Workshop.GeneralRepository;

import Workshop.CommunicationChannel.ServerComm;
import  Workshop.Messages.GeneralRepositoryMessages;

/**
 * <p>
 * Proxy to receive method calls of the General repository and process them on a new thread (so it can attend multiple requests at the same time).
 * </p>
 * @author Dinis Canastro
 * @author Bruno Mendes
 */
public class GeneralRepositoryProxy implements Runnable{
    ServerComm socket;
    GeneralRepository gr;

    GeneralRepositoryProxy(ServerComm socketCommunication, GeneralRepository gr) {
        this.socket = socketCommunication;
        this.gr = gr;
    }

    @Override
    public void run() {
        GeneralRepositoryMessages msg_received, msg_sent = null;
        
        msg_received = (GeneralRepositoryMessages) socket.readObject();
        System.out.println("MSG RECEIVED: " + msg_received.getMsgType());
        
        int response_int;
        int[] response_array;
        
        // Processing the message method
        switch (msg_received.getMsgType()) {
            case GeneralRepositoryMessages.SET_MANAGER_STATE:
                gr.setManagerState(msg_received.getstate());
                msg_sent = new GeneralRepositoryMessages(GeneralRepositoryMessages.OK);
                break;
                
                
            case GeneralRepositoryMessages.SET_CUSTOMER_STATE:
                gr.setCustomerState(msg_received.getstate(),msg_received.getThreadID());
                msg_sent = new GeneralRepositoryMessages(GeneralRepositoryMessages.OK);
                break;

            case GeneralRepositoryMessages.SET_MECHANIC_STATE:
                gr.setMechanicState(msg_received.getstate(),msg_received.getThreadID());
                msg_sent = new GeneralRepositoryMessages(GeneralRepositoryMessages.OK);
                break;
                
                
            case GeneralRepositoryMessages.SET_CUSTOMER_CAR:
                gr.setCustomerCar(msg_received.getstate(),msg_received.getThreadID());
                msg_sent = new GeneralRepositoryMessages(GeneralRepositoryMessages.OK);
                break;
                
            case GeneralRepositoryMessages.SET_REPLACEMENT_NEEDED:
                gr.setReplacementNeeded(msg_received.getstate(),msg_received.getThreadID());
                msg_sent = new GeneralRepositoryMessages(GeneralRepositoryMessages.OK);
                break;
                
            case GeneralRepositoryMessages.SET_REPAIR_DONE:
                gr.setRepairDone(msg_received.getstate(),msg_received.getThreadID());
                msg_sent = new GeneralRepositoryMessages(GeneralRepositoryMessages.OK);
                break;
            
            case GeneralRepositoryMessages.SET_ENTRANCE_QUEUE:
                gr.setEntranceQueue(msg_received.getEntrance_qq());
                msg_sent = new GeneralRepositoryMessages(GeneralRepositoryMessages.OK);
                break;  
                
            case GeneralRepositoryMessages.SET_REPLACEMENT_QUEUE:
                gr.setReplacementQueue(msg_received.getReplacement_qq());
                msg_sent = new GeneralRepositoryMessages(GeneralRepositoryMessages.OK);
                break;
                
            case GeneralRepositoryMessages.INCREASE_REPAIR_COUNT:
                gr.increaseRepairedCount();
                msg_sent = new GeneralRepositoryMessages(GeneralRepositoryMessages.OK);
                break;
            
            case GeneralRepositoryMessages.INCREASE_CUSTOMER_PARKED:
                gr.increaseCustomerParked();
                msg_sent = new GeneralRepositoryMessages(GeneralRepositoryMessages.OK);
                break;
                
            case GeneralRepositoryMessages.DECREASE_CUSTOMER_PARKED:
                gr.decreaseCustomerParked();
                msg_sent = new GeneralRepositoryMessages(GeneralRepositoryMessages.OK);
                break;
                
            case GeneralRepositoryMessages.INCREASE_MANAGER_REQUESTS:
                gr.increaseManagerRequests();
                msg_sent = new GeneralRepositoryMessages(GeneralRepositoryMessages.OK);
                break;
               
            case GeneralRepositoryMessages.SET_PART_STOCK:
                gr.setPartStock(msg_received.getPart_stock());
                msg_sent = new GeneralRepositoryMessages(GeneralRepositoryMessages.OK);
                break;    
            
            case GeneralRepositoryMessages.SET_PART_REQUIREMENTS:
                gr.setPartRequirements(msg_received.getPart_required());
                msg_sent = new GeneralRepositoryMessages(GeneralRepositoryMessages.OK);
                break;  
                
            case GeneralRepositoryMessages.SET_NOTIFICATIONS:
                gr.setNotifications(msg_received.getManager_notified());
                msg_sent = new GeneralRepositoryMessages(GeneralRepositoryMessages.OK);
                break;  
            
            case GeneralRepositoryMessages.SET_ORDERS_FILLED:
                gr.setOrdersFilled(msg_received.getParts_ordered());
                msg_sent = new GeneralRepositoryMessages(GeneralRepositoryMessages.OK);
                break;
            
            case GeneralRepositoryMessages.INCREASE_REPLACEMENT_PARKED:
                gr.increaseReplacementParked();
                msg_sent = new GeneralRepositoryMessages(GeneralRepositoryMessages.OK);
                break;
                
            case GeneralRepositoryMessages.DECREASE_REPLACEMENT_PARKED:
                gr.decreaseReplacementParked();
                msg_sent = new GeneralRepositoryMessages(GeneralRepositoryMessages.OK);
                break;

            case GeneralRepositoryMessages.END:
                System.out.println("END");
                gr.setDone();
                msg_sent = new GeneralRepositoryMessages(GeneralRepositoryMessages.OK);
                socket.writeObject(msg_sent);
                socket.close();
                throw new RuntimeException("Received end message.");
            default:
                System.out.println("[ERROR] Received invalid message type: " + msg_received.getMsgType());
                break;
        }
        
        socket.writeObject(msg_sent);
        socket.close();
    }
}
