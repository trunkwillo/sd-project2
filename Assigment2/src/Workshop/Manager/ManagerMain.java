/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Workshop.Manager;

/**
 * <p>
 * Main class for launching a single Manager thread.
 * </p>
 * @author Dinis Canastro
 * @author Bruno Mendes
 */
public class ManagerMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int Manager_Number = 1;
        Thread[] threads = new Thread[1];
        
        for(int t = 0; t < threads.length; t++){
            Runnable m_runnable = new Manager(t);
            threads[t] = new Thread(m_runnable);
            threads[t].start();
        }
        
        for(int t = 0; t < threads.length; t++){
            try{
                threads[t].join();
            } catch (InterruptedException ex) { 
                System.out.println("Deu ruim!: " + ex.toString());
                System.exit(1);
            }
        }       
    }
    
}
