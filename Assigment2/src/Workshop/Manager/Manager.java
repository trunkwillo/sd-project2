/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Workshop.Manager;

import Workshop.CommunicationChannel.ClientComm;
import static Workshop.CommunicationChannel.CommPorts.*;
        
        
/**
 * <p>
 * Manager thread containing his life cycle and state information.
 * </p>
 * @author Dinis Canastro
 * @author Bruno Mendes
 */
public class Manager implements Runnable{
    
    private int threadID;
    int post_it_description;
    int post_it_info;
    ManagerMethods mm = new ManagerMethods();
    int customer_count = 0;
    

    public Manager(int threadID){
      this.threadID = threadID;
    }
    
    /**
 * <p>
 *  Manager life cycle
 * </p>
 */
    @Override
    public void run(){
        System.out.println("[INFO] Manager started!");
        while(customer_count < 30){
            mm.getNextTask(threadID);
            System.out.println("[INFO] Manager waiting for tasks!");
            switch(mm.appraiseSit(threadID)){
                case 1: // Talk with customers
                    //gr.setManagerState("ATC");
                    System.out.println("[INFO] Manager talking with Customer!");
                    int decision = mm.talkToCustomer(threadID);
                    if(decision >= 0 && decision <= 29){          // ID of car to repair
                        System.out.println("[INFO] Manager talking with Customer for repair job!");
                        mm.registerService(decision,threadID);
                        //gr.setManagerState("PSJ");
                    }else if(decision == -1){   // Receive Payment
                        System.out.println("[INFO] Manager talking with Customer for payment!");
                        mm.receivePayment(threadID);
                        customer_count++;
                    }
                    break;
                case 2: // Stock to refill
                    System.out.println("[INFO] Manager going to supplier site!");
                    int[] order = mm.getPartsOrders(threadID);
                    // Get more parts than the requireds
                    order = addExtra(order);
                    if(!mm.goToSupplierSite(order,threadID)){
                        throw new RuntimeException("Suplier site failed horribly!");
                    }
                    //gr.setManagerState("GNP");
                    mm.storePart(order,threadID);
                    //gr.setManagerState("RPS");
                    break;
                case 3: // Customers to notify
                    System.out.println("[INFO] Manager notifying Customer!");
                    int tmp = mm.notifyClient(threadID);
                    if(tmp == -1){
                        break;
                    }
                    mm.phoneCustomer(tmp,threadID);
                    //gr.setManagerState("ALC");
                    break;
                case 4: // Hand car keys 
                    System.out.println("[INFO] Manager handing replacement keys to Customer!");
                    //gr.setManagerState("ATC");
                    mm.handCarKey(threadID);
                    break;
                default:
                    throw new RuntimeException("Task with wrong or undefined description");
            }
            //gr.setManagerState("CWD");
        }
        System.out.println("[END] Manager finished!");
        // Closing all monitors
        mm.closeAll(threadID);
    }
    
    // ---------------------- Auxiliary Methods -------------------------
    
    /**
    * <p> 
    *   Increases the amount of parts on the order.
    *   This decreases the amount of trips made to the supplier site.
    * </p>
    * @param order has information relative to the quantity required of each part.
    * @return Returns the current required order list increased by the extra amount.
    */
        private int[] addExtra(int[] order){
            for(int i = 0; i < order.length; i++){
                order[i] += 5;
            }
            return order;
        }
    
    
}
