/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Workshop.Manager;

import Workshop.CommunicationChannel.ClientComm;
import static Workshop.CommunicationChannel.CommPorts.*;
import Workshop.Interfaces.ManagerLoungeInterface;
import Workshop.Interfaces.ManagerOutsideWorldInterface;
import Workshop.Interfaces.ManagerRepairAreaInterface;
import Workshop.Interfaces.ManagerSupplierSiteInterface;

import Workshop.Messages.GeneralRepositoryMessages;
import Workshop.Messages.LoungeMessages;
import Workshop.Messages.OutsideWorldMessages;
import Workshop.Messages.ParkMessages;
import Workshop.Messages.RepairAreaMessages;
import Workshop.Messages.SupplierSiteMessages;

/**
 * <p>
 * Manager client auxiliary class to send requests to the various interacting servers
 * </p>
 * @author Dinis Canastro
 * @author Bruno Mendes
 */
public class ManagerMethods{ //implements ManagerLoungeInterface, ManagerOutsideWorldInterface, ManagerRepairAreaInterface, ManagerSupplierSiteInterface{
   
     
    ClientComm gr = new ClientComm(GENERAL_REPOSITORY_NAME,GENERAL_REPOSITORY_PORT);
    ClientComm lg = new ClientComm(LOUNGE_NAME, LOUNGE_PORT);
    ClientComm ra = new ClientComm(REPAIR_AREA_NAME, REPAIR_AREA_PORT);
    ClientComm pk = new ClientComm(PARK_NAME, PARK_PORT);
    ClientComm ow = new ClientComm(OUTSIDE_WORLD_NAME, OUTSIDE_WORLD_PORT);
    ClientComm ss = new ClientComm(SUPPLIER_SITE_NAME, SUPPLIER_SITE_PORT);
    
    /***
     * [Lounge]
     */
    //@Override
    public void getNextTask(int threadID){
        LoungeMessages response;
        openComm(lg,"Manager "+ threadID +": Lounge");
        lg.writeObject(new LoungeMessages(LoungeMessages.GET_NEXT_TASK));
        response = (LoungeMessages) lg.readObject(); 
        lg.close();
    }
    
    /***
     * [Lounge]
     */
//    @Override
    public int appraiseSit(int threadID){
        LoungeMessages response;
        openComm(lg,"Manager "+ threadID +": Lounge");
        lg.writeObject(new LoungeMessages(LoungeMessages.APPRAISE_SITUATION));
        response = (LoungeMessages) lg.readObject(); 
        lg.close();
        return response.getMessageResponse();
    }
    
    /***
     * [Lounge]
     */
//    @Override
    public int talkToCustomer(int threadID){
        LoungeMessages response;
        openComm(lg,"Manager "+ threadID +": Lounge");
        lg.writeObject(new LoungeMessages(LoungeMessages.TALK_TO_CUSTOMER));
        response = (LoungeMessages) lg.readObject(); 
        lg.close();
        return response.getMessageResponse();
    }
    
    /***
     * [Repair Area]
     */
//    @Override
    public void registerService(int carID,int threadID){
        RepairAreaMessages response;
        openComm(ra,"Manager "+ threadID +": Repair Area");
        ra.writeObject(new RepairAreaMessages(RepairAreaMessages.REGISTER_SERVICE,carID));
        response = (RepairAreaMessages) ra.readObject(); 
        ra.close();
    }
    
    /***
     * [Lounge]
     */
//    @Override
    public void receivePayment(int threadID){
        LoungeMessages response;
        openComm(lg,"Manager "+ threadID +": Lounge");
        lg.writeObject(new LoungeMessages(LoungeMessages.RECEIVE_PAYMENT));
        response = (LoungeMessages) lg.readObject(); 
        lg.close();
    }
    
    /***
     * [Lounge]
     */
//    @Override
    public int[] getPartsOrders(int threadID){
        LoungeMessages response;
        openComm(lg,"Manager "+ threadID +": Lounge");
        lg.writeObject(new LoungeMessages(LoungeMessages.GET_PARTS_ORDERS));
        response = (LoungeMessages) lg.readObject(); 
        lg.close();
        return response.getMessageResponseArray();
    }
    
    /***
     * [Supplier Site]
     */
//    @Override
    public boolean goToSupplierSite(int[] order, int threadID){
        SupplierSiteMessages response;
        openComm(ss,"Manager "+ threadID +": Supplier Site");
        ss.writeObject(new SupplierSiteMessages(SupplierSiteMessages.GO_TO_SUPPLIER_SITE,order));
        response = (SupplierSiteMessages) ss.readObject(); 
        ss.close();
        return response.getMessageResponse();
    }
    
    /***
     * [Repair Area]
     */
//    @Override
    public void storePart(int[] parts,int threadID){
        RepairAreaMessages response;
        openComm(ra,"Manager "+ threadID +": Repair Area");
        ra.writeObject(new RepairAreaMessages(RepairAreaMessages.STORE_PART,parts));
        response = (RepairAreaMessages) ra.readObject(); 
        ra.close();
    }
    
    /***
     * [Lounge]
     */
 //   @Override
    public int notifyClient(int threadID){
        LoungeMessages response;
        openComm(lg,"Manager "+ threadID +": Lounge");
        lg.writeObject(new LoungeMessages(LoungeMessages.NOTIFY_CLIENT));
        response = (LoungeMessages) lg.readObject(); 
        lg.close();
        return response.getMessageResponse();
    }
    
    /***
     * [Outside World]
     */
//    @Override
    public void phoneCustomer(int CustomerID,int threadID){
        OutsideWorldMessages response;
        openComm(ow,"Manager "+ threadID +": Outside World");
        ow.writeObject(new OutsideWorldMessages(OutsideWorldMessages.PHONE_CUSTOMER,CustomerID));
        response = (OutsideWorldMessages) ow.readObject(); 
        ow.close();
    }
    
    /***
     * [Lounge]
     */
//    @Override
    public void handCarKey(int threadID){
        LoungeMessages response;
        openComm(lg,"Manager "+ threadID +": Lounge");
        lg.writeObject(new LoungeMessages(LoungeMessages.HAND_CAR_KEY));
        response = (LoungeMessages) lg.readObject(); 
        lg.close();
        
        
    }
    
    private void openComm(ClientComm cc, String name){
        while(!cc.open()){
            System.out.println(name + " not open, trying again...");
            try{
                Thread.sleep(1000);
            }catch(Exception ex){ }            
        }
    }

    public void closeAll(int threadID){
        // Lounge
        System.out.println("Closing Lounge");
        LoungeMessages response_lg;
        openComm(lg,"Manager "+ threadID +": Closing Lounge");
        lg.writeObject(new LoungeMessages(LoungeMessages.END));
        response_lg = (LoungeMessages) lg.readObject(); 
        lg.close();
        
        // Repair Area
        System.out.println("Repair Area Lounge");
        RepairAreaMessages response_ra;
        openComm(ra,"Manager "+ threadID +": Closing Repair Area");
        ra.writeObject(new RepairAreaMessages(RepairAreaMessages.END));
        response_ra = (RepairAreaMessages) ra.readObject(); 
        ra.close();
        
        // Supplier Site
        System.out.println("Supplier Site Lounge");
        SupplierSiteMessages response_ss;
        openComm(ss,"Manager "+ threadID +": Closing Supplier Site");
        ss.writeObject(new SupplierSiteMessages(SupplierSiteMessages.END));
        response_ss = (SupplierSiteMessages) ss.readObject(); 
        ss.close();
        
        // Outside World
        System.out.println("Outside World Lounge");
        OutsideWorldMessages response_ow;
        openComm(ow,"Manager "+ threadID +": Closing Outside World");
        ow.writeObject(new OutsideWorldMessages(OutsideWorldMessages.END));
        response_ow = (OutsideWorldMessages) ow.readObject(); 
        ow.close();
        
        // Park
        System.out.println("Park Lounge");
        ParkMessages response_pk;
        openComm(pk,"Manager "+ threadID +": Closing Park");
        pk.writeObject(new ParkMessages(ParkMessages.END));
        response_pk = (ParkMessages) pk.readObject(); 
        pk.close();
        
        // General Repository
        System.out.println("General Repository Lounge");
        GeneralRepositoryMessages response_gr;
        openComm(gr,"Manager "+ threadID +": Closing General Repository");
        gr.writeObject(new GeneralRepositoryMessages(GeneralRepositoryMessages.END));
        response_gr = (GeneralRepositoryMessages) gr.readObject(); 
        gr.close();
        
        
    }
    
}
