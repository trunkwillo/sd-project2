/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Workshop.OutsideWorld;

import Workshop.CommunicationChannel.ServerComm;
import Workshop.Messages.OutsideWorldMessages;

/**
 * Instance of Outside World Proxy thread that retrieves and processes a single request on a different thread of execution.
 * @author Dinis Canastro
 * @author Bruno Mendes
 */
public class OutsideWorldProxy implements Runnable{

    ServerComm cc;
    OutsideWorld ow ;
    /**
     * Outside World  instantiation
     * 
     * @param socketCommunication Communication channel
     * 
     */
    public OutsideWorldProxy(ServerComm socketCommunication, OutsideWorld ow ) {
        this.cc = socketCommunication;
        this.ow = ow;
    }

    

    /**
     * Life cycle
     */
    @Override
    public void run() {
        OutsideWorldMessages msg_received,msg_sent = null;
        
        
        msg_received = (OutsideWorldMessages) cc.readObject();
        System.out.println("MSG RECEIVED: " + msg_received.getMessageType());
        
        switch (msg_received.getMessageType()) {
            case OutsideWorldMessages.DECIDE_ON_REPAIR :
                ow.decideOnRepair();
                System.out.println("Sending answer to DECIDE ON REPAIR");
                msg_sent = new OutsideWorldMessages(OutsideWorldMessages.OK);
                break;
                
            case OutsideWorldMessages.BACK_TO_WORK_BY_CAR :
                ow.backtoWorkByCar(msg_received.getMessageThreadID());
                msg_sent = new OutsideWorldMessages(OutsideWorldMessages.OK);
                break;
                
            case OutsideWorldMessages.BACK_TO_WORK_BY_BUS :
                ow.backtoWorkByBus(msg_received.getMessageThreadID());
                msg_sent = new OutsideWorldMessages(OutsideWorldMessages.OK);
                break;
         
            case OutsideWorldMessages.PHONE_CUSTOMER :
                ow.phoneCustomer(msg_received.getMessageOwner());
                msg_sent = new OutsideWorldMessages(OutsideWorldMessages.OK);
                break;

            case OutsideWorldMessages.END:
               System.out.println("END");
               ow.setDone();
               msg_sent = new OutsideWorldMessages(OutsideWorldMessages.OK);
               cc.writeObject(msg_sent);
                cc.close();
               throw new RuntimeException("Received end message.");
                 
            default:
                System.out.println("[ERROR] Received invalid message type: " + msg_received.getMessageType());
                break;
        
        }
        
        cc.writeObject(msg_sent);
        cc.close();
    }
    

}
