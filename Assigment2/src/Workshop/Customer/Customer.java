/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Workshop.Customer;

import Workshop.CommunicationChannel.ClientComm;
import static Workshop.CommunicationChannel.CommPorts.*;
import java.util.Random;
/**
 *
 * @author bmend
 */
public class Customer implements Runnable {
    private int threadID;
    private ClientComm gr ;
    private ClientComm lg;
    private ClientComm ow;
    private ClientComm pk;
    private int c;
    private boolean wantRepCar;
    private boolean hasRepCar;
    private int rep_car= -1;
    private int c_temp;
    private CustomerMethods cc = new CustomerMethods();
    
    /**
     * Requires only the current thread ID so it can be tracked which one it is in a distributed environment
     * @param threadID the current thread identification unique number.
     */
    public Customer(int threadID ){
        this.threadID = threadID;
        this.gr = new ClientComm(GENERAL_REPOSITORY_NAME,GENERAL_REPOSITORY_PORT);
        this.lg = new ClientComm(LOUNGE_NAME, LOUNGE_PORT);
        this.ow = new ClientComm(OUTSIDE_WORLD_NAME, OUTSIDE_WORLD_PORT);
        this.pk = new ClientComm(PARK_NAME, PARK_PORT);
        this.c = threadID;
        this.wantRepCar = repCar();
    }
    
    /**
     * Customer life cycle with the multiple path options.
     */
    @Override
    public void run(){
        System.out.println("[INFO] Customer started! " + threadID);
        if(wantRepCar) cc.setReplacementNeeded("T", threadID);
        System.out.println("[INFO] Customer decided to go to repair! " + threadID);
        cc.decideOnRepair(threadID); // wait
        System.out.println("[INFO] Customer goes to repair shop! " + threadID);
        cc.goToRepairShop(threadID, threadID);
        //gr.setCustomerState("PRK", threadID);
        System.out.println("[INFO] Customer queues in! " + threadID);
        cc.queueIn(threadID);//wait
        //gr.setCustomerState("RCP", threadID);
        System.out.println("[INFO] Customer talks with manager for repair job! " + threadID);
        cc.talkWithManager(threadID); 
        if(this.wantRepCar){
            System.out.println("[INFO] Customer wants replacement car! " + threadID);
            rep_car = cc.collectKey(threadID);
            //gr.setCustomerCar(String.valueOf(rep_car), threadID);
            //gr.setCustomerState("WRC", threadID);
            System.out.println("[INFO] Customer has replacement car key! " + threadID);
            cc.findCar(rep_car, threadID);
            //gr.setCustomerState("PRK", threadID);
            System.out.println("[INFO] Customer goes back to work by car! " + threadID);
            cc.backtoWorkByCar(threadID);
            //gr.setCustomerState("NLC", threadID);
            System.out.println("[INFO] Customer goes back to repair shop! " + threadID);
            cc.goToRepairShop(rep_car, threadID);
            //gr.setCustomerState("PRK", threadID);
        }else{
            System.out.println("[INFO] Customer goes back by bus! " + threadID);
            cc.backtoWorkByBus(threadID);
            //gr.setCustomerCar("-", threadID);
            //gr.setCustomerState("NLO", threadID);
        }
        //gr.setRepairDone("T", threadID);
        System.out.println("[INFO] Customer queues in AGAIN! " + threadID);
        cc.queueIn(threadID);
        //gr.setCustomerState("RCP", threadID);
        System.out.println("[INFO] Customer pays for the service! " + threadID);
        cc.payForTheService(rep_car,threadID);
        System.out.println("[INFO] Customer collects car! " + threadID);
        c_temp = cc.collectCar(threadID);
        //gr.setCustomerCar(String.valueOf(threadID), threadID);
        //gr.setCustomerState("PRK", threadID);
        if (c != c_temp )  {
            throw new RuntimeException("Returned Wrong Car! C:  " + c + " C_temp: " + c_temp);
        }
        System.out.println("[INFO] Customer finally leaves! " + threadID);
        cc.backtoWorkByCar(threadID);
        //gr.setCustomerState("NLC", threadID);
    }


// ------------------------------------------------------------
// ----------------- AUX FUNCS --------------------------------

    /**
    * <p> Method to decide wether a Customer will require a replacement car or not . </p> 
    * @return True or False to have or not replacement car
    */
    public boolean repCar(){
        return probability();
    }

    /**
    * <p> If are substituion cars to give to the client, this flag is put at True.</p> 
    */
    public void set_hasRepCar(){
        this.hasRepCar = true;
    }



    /**
    * <p> Getter for replacement car requirements of this instance of customer.</p> 
    */
    public boolean get_hasRepCar(){
        return this.hasRepCar;
    }

    /**
    * <p> Method that generates a random value and decides if it's true or false according to a defined threshold.</p> 
    * @return True if the probability is higher than 80%, false if below.
    */
    private boolean probability(){
        Random gerador = new Random();
        Double prob = 0.0;
        prob = gerador.nextDouble();
        if(prob > 0.8){
            return true;
        }
        else{
            return false;
        }
    }
}

    
    

