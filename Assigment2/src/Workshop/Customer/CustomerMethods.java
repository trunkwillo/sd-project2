/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Workshop.Customer;

import Workshop.CommunicationChannel.ClientComm;
import static Workshop.CommunicationChannel.CommPorts.*;

import Workshop.Messages.GeneralRepositoryMessages;
import Workshop.Messages.LoungeMessages;
import Workshop.Messages.OutsideWorldMessages;
import Workshop.Messages.ParkMessages;
/**
 *
 * @author bmend
 */
public class CustomerMethods {
    // variables
    ClientComm gr = new ClientComm(GENERAL_REPOSITORY_NAME,GENERAL_REPOSITORY_PORT);
    ClientComm lg = new ClientComm(LOUNGE_NAME, LOUNGE_PORT);
    ClientComm ow = new ClientComm(OUTSIDE_WORLD_NAME, OUTSIDE_WORLD_PORT);
    ClientComm pk = new ClientComm(PARK_NAME, PARK_PORT);
    
    
    // ############# General Repository ##################
    /**
     * Method to send to General Repository Customer's Replacement car needs (either 'True' or 'False')
     * @param state defines if it's either True or False in Logger shape ('T' or 'F')
     * @param threadID identifies the Customer Thread Identification
     */
    public void setReplacementNeeded( String state, int threadID){
        GeneralRepositoryMessages response;
        openComm(gr,"Customer "+ threadID +": General Repository");
        gr.writeObject(new GeneralRepositoryMessages(GeneralRepositoryMessages.SET_REPLACEMENT_NEEDED,state,threadID));
        response = (GeneralRepositoryMessages) gr.readObject(); 
        gr.close();
    }
    
    
    
    // ############## Lounge ##################
    /**
    * <p> 
    * Method to enter the queue for the reception in the Lounge
    * </p>
    * @param threadID Identifies the Customer Thread Identification
    */
    public void queueIn(int threadID){
        LoungeMessages response;
        openComm(lg,"Customer "+ threadID +": Lounge");
        lg.writeObject(new LoungeMessages(LoungeMessages.QUEUE_IN,threadID));
        response = (LoungeMessages) lg.readObject(); 
        lg.close();
    }
    
    /**
    * <p> 
    * Method for Customer to share with the Manager the need for a repair job on their car.
    * </p>
    * @param threadID Identifies the Customer Thread Identification
    */
    public  void talkWithManager(int threadID){
        LoungeMessages response;
        openComm(lg,"Customer "+ threadID +": Lounge");
        lg.writeObject(new LoungeMessages(LoungeMessages.TALK_WITH_MANAGER,threadID));
        response = (LoungeMessages) lg.readObject();
        lg.close();
    }
    
    /**
    * <p> 
    * Method for Customer to share with the Manager that he is paying for a repair job.
    * </p>
    * @param replacement explicits whether the Customer is delivering a replacement car and, if he is returning a replacement car, what ID is it.
    * @param threadID Identifies the Customer Thread Identification
    */
    public void payForTheService(int replacement,int threadID){
        LoungeMessages response;
        openComm(lg,"Customer "+ threadID +": Lounge");
        lg.writeObject(new LoungeMessages(LoungeMessages.PAY_FOR_SERVICE,threadID,replacement));
        response = (LoungeMessages) lg.readObject(); 
        lg.close();
    
    }
    
    
    /**
    * <p> 
    * Method for Customer to queue in waiting for a replacement car key.
    * </p>
    * @param threadID Identifies the Customer Thread Identification
    */
    public  int collectKey(int threadID){
        LoungeMessages response;
        openComm(lg,"Customer "+ threadID +": Lounge");
        lg.writeObject(new LoungeMessages(LoungeMessages.COLLECT_KEY,threadID));
        response = (LoungeMessages) lg.readObject(); 
        lg.close();
        return response.getMessageResponse();
    }
    
    // ############## OutSide World ##################
    
    /**
    * <p> 
    * Method that defines either if a Customer is ready to require a repair job.
    * </p>
    * Used for randomization of customers entry.
    * @param threadID Identifies the Customer Thread Identification
    */
    public void decideOnRepair(int threadID){
        OutsideWorldMessages response;
        openComm(ow,"Customer "+ threadID +": Outside World");
        ow.writeObject(new OutsideWorldMessages(OutsideWorldMessages.DECIDE_ON_REPAIR));
        response = (OutsideWorldMessages) ow.readObject();
        System.out.println("Got response from outside world" + response.getMessageType());
        ow.close();
    }
    
    /**
    * <p> 
    * Method for Customer to return to the outside world with a car.
    * </p>
    * @param threadID Identifies the Customer Thread Identification
    */
    public void backtoWorkByCar(int threadID){
        OutsideWorldMessages response;
        openComm(ow,"Customer "+ threadID +": Outside World");
        ow.writeObject(new OutsideWorldMessages(OutsideWorldMessages.BACK_TO_WORK_BY_CAR,threadID));
        response = (OutsideWorldMessages) ow.readObject(); 
        ow.close();
    }
    
    /**
    * <p> 
    * Method for Customer to return to the outside world without a car.
    * </p>
    * @param threadID Identifies the Customer Thread Identification
    */
    public void backtoWorkByBus(int threadID){
        OutsideWorldMessages response;
        openComm(ow,"Customer "+ threadID +": Outside World");
        ow.writeObject(new OutsideWorldMessages(OutsideWorldMessages.BACK_TO_WORK_BY_BUS,threadID));
        response = (OutsideWorldMessages) ow.readObject(); 
        ow.close();
    
    }
    
    // ############## Park ##################
    
    /**
    * <p> 
    * Method for Customer to enter the Repair Shop's Park.
    * </p>
    * @param threadID Identifies the Customer Thread Identification
    */
    public boolean goToRepairShop(int car, int threadID){
        ParkMessages response;
        openComm(pk,"Customer "+ threadID +": Park");
        pk.writeObject(new ParkMessages(ParkMessages.GO_TO_REPAIR_SHOP,threadID,car));
        response = (ParkMessages) pk.readObject(); 
        pk.close();
        return response.getMessageResponseBool();
    }
    
    /**
    * <p> 
    * Method for Customer to return to their vehicle in the Repair Shop's Park.
    * </p>
    * @param threadID Identifies the Customer Thread Identification
    */
    public boolean findCar(int car, int threadID) {
        ParkMessages response;
        openComm(pk,"Customer "+ threadID +": Park");
        pk.writeObject(new ParkMessages(ParkMessages.FIND_CAR,threadID,car));
        response = (ParkMessages) pk.readObject(); 
        pk.close();
        return response.getMessageState();
    }
    
    /**
    * <p> 
    * Method for Customer to find the replacement vehicle in the Repair Shop's Park.
    * </p>
    * @param threadID Identifies the Customer Thread Identification
    */
    public int collectCar(int threadID){
        ParkMessages response;
        openComm(pk,"Customer "+ threadID +": Park");
        pk.writeObject(new ParkMessages(ParkMessages.COLLECT_CAR,threadID));
        response = (ParkMessages) pk.readObject(); 
        pk.close();
        return response.getMessageCar();
    }
    
     /**
     * Generic method to open the communication to the server.
     * In case of failure keeps trying to initiate communication in 1 second intervals
     * 
     * @param cc the communication channel to open the connection
     * @param name Name of the monitor corresponding to the channel
     */
    private void openComm(ClientComm cc, String name){
        while(!cc.open()){
            System.out.println(name + " not open, trying again...");
            try{
                Thread.sleep(1000);
            }catch(Exception ex){ }            
        }
    }
    
}
