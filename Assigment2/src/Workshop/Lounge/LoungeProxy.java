/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Workshop.Lounge;

import Workshop.CommunicationChannel.ServerComm;
import Workshop.Messages.LoungeMessages;

/**
 * Instance of Lounge Proxy thread that retrieves and processes a single request on a different thread of execution.
 * @author Dinis Canastro
 * @author Bruno Mendes
 */
public class LoungeProxy implements Runnable{
    
    // Communication
    ServerComm socket;
    Lounge lg;
    
    public LoungeProxy(ServerComm socket, Lounge lg) {
        this.socket = socket;
        this.lg = lg;
    }
    
    
    @Override
    public void run(){
        
        
        LoungeMessages msg_received, msg_sent = null;
        
        msg_received = (LoungeMessages) socket.readObject();
        System.out.println("MSG RECEIVED: " + msg_received.getMessageType());
        
        int response_int;
        int[] response_array;
        
        // Processing the message method
        switch (msg_received.getMessageType()) {
            case LoungeMessages.GET_NEXT_TASK:
                lg.getNextTask();
                msg_sent = new LoungeMessages(LoungeMessages.OK);
                break;

            case LoungeMessages.APPRAISE_SITUATION:
                response_int = lg.appraiseSit();
                System.out.println("GOING TO DO: " + response_int);
                msg_sent = new LoungeMessages(LoungeMessages.OK, response_int);
                break;

            case LoungeMessages.TALK_TO_CUSTOMER:
                response_int = lg.talkToCustomer();
                msg_sent = new LoungeMessages(LoungeMessages.OK, response_int);
                break;

            case LoungeMessages.RECEIVE_PAYMENT:
                lg.receivePayment();
                msg_sent = new LoungeMessages(LoungeMessages.OK);
                break;

            case LoungeMessages.HAND_CAR_KEY:
                lg.handCarKey();
                msg_sent = new LoungeMessages(LoungeMessages.OK);
                break;

            case LoungeMessages.QUEUE_IN:
                lg.queueIn(msg_received.getMessageThreadID());
                msg_sent = new LoungeMessages(LoungeMessages.OK);
                break;

            case LoungeMessages.TALK_WITH_MANAGER:
                lg.talkWithManager(msg_received.getMessageThreadID());
                msg_sent = new LoungeMessages(LoungeMessages.OK);
                break;

            case LoungeMessages.PAY_FOR_SERVICE:
                lg.payForTheService(msg_received.getMessageReplacement());
                msg_sent = new LoungeMessages(LoungeMessages.OK);
                break;

            case LoungeMessages.COLLECT_KEY:
                response_int = lg.collectKey(msg_received.getMessageThreadID());
                msg_sent = new LoungeMessages(LoungeMessages.OK, response_int);
                break;

            case LoungeMessages.LET_MANAGER_KNOW:
                lg.letManagerKnow(msg_received.getMessageStock(), msg_received.getMessageThreadID());
                msg_sent = new LoungeMessages(LoungeMessages.OK);
                break;

            case LoungeMessages.REPAIR_CONCLUDED:
                lg.repairConcluded(msg_received.getMessageCar(), msg_received.getMessageThreadID());
                msg_sent = new LoungeMessages(LoungeMessages.OK);
                break;

            case LoungeMessages.NOTIFY_CLIENT:
                response_int = lg.notifyClient();
                msg_sent = new LoungeMessages(LoungeMessages.OK, response_int);
                break;

            case LoungeMessages.GET_PARTS_ORDERS:
                response_array = lg.getPartsOrders();
                msg_sent = new LoungeMessages(LoungeMessages.OK, response_array);
                break;

            case LoungeMessages.END:
                System.out.println("END");
                lg.setDone();
                msg_sent = new LoungeMessages(LoungeMessages.OK);
                socket.writeObject(msg_sent);
                socket.close();
                throw new RuntimeException("Received end message.");
            default:
                System.out.println("[ERROR] Received invalid message type: " + msg_received.getMessageType());
                break;
        }
        
        socket.writeObject(msg_sent);
        socket.close();
    }
}
