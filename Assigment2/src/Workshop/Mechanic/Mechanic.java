/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Workshop.Mechanic;

import Workshop.CommunicationChannel.ClientComm;
import static Workshop.CommunicationChannel.CommPorts.*;
/**
 * <p>
 * Mechanic thread containing his life cycle and state information.
 * </p>
 * @author Dinis Canastro
 * @author Bruno Mendes
 */
public class Mechanic implements Runnable{
    private int threadID;
    private MechanicMethods mm = new MechanicMethods();
    private ClientComm gr ;
    private ClientComm lg;
    private ClientComm ra;
    private ClientComm pk;
    int nparts = 3;
    int[] car = new int[1+nparts];
    int[] part_toRepair = new int[nparts];
  
  

  public Mechanic(int threadID){
    this.threadID = threadID;
    this.gr = new ClientComm(GENERAL_REPOSITORY_NAME,GENERAL_REPOSITORY_PORT);
    this.lg = new ClientComm(LOUNGE_NAME, LOUNGE_PORT);
    this.ra = new ClientComm(REPAIR_AREA_NAME, REPAIR_AREA_PORT);
    this.pk = new ClientComm(PARK_NAME, PARK_PORT);
  }

  @Override
  public void run(){
    System.out.println("[INFO] Mechanic started!");
    while(mm.workdone(threadID)){
        System.out.println("[INFO] Mechanic reading the paper!");
        mm.readThePaper(threadID);
        System.out.println("[INFO] Mechanic going to repair car!");
        this.car = mm.startRepairProcedure(threadID);
        if(this.car[0] == -1){
            continue;
        }
        //gr.setMechanicState("FTC", threadID);
        System.out.println("[INFO] Mechanic going to get car!");
        mm.getVehicle(this.car[0],threadID);
        System.out.println("[INFO] Mechanic verifying if parts are available!");
        int[] order = mm.partAvailable(this.car, threadID);
        //gr.setMechanicState("CST", threadID);
        if(order[0] == -1){
            System.out.println("[INFO] Mechanic getting the parts!");
            part_toRepair = mm.getRequiredPart(this.car,this.threadID);
            mm.resumeRepairProcedure(threadID);
            //gr.setMechanicState("FTC", threadID);
            System.out.println("[INFO] Mechanic repairing car!");
            this.car = mm.fixIt(this.car, this.part_toRepair,this.threadID);
            System.out.println("[INFO] Mechanic returning repaired car!");
            mm.returnVehicle(this.car[0],threadID);
            System.out.println("[INFO] Mechanic warns repair is done!");
            mm.repairConcluded(this.car[0], threadID);
            //gr.setMechanicState("ALM", threadID);
        }
        else{
            System.out.println("[INFO] Mechanic suspends repair!");
            mm.putCarWaiting(this.car,this.threadID);
            System.out.println("[INFO] Mechanic warns manager there are no parts!");
            mm.letManagerKnow(order, threadID); 
            //gr.setMechanicState("ALM", threadID);
        }
        //gr.setMechanicState("WFW", threadID);
    }
    System.out.println("[INFO] Mechanic ended!");
    mm.wakeUpPal(threadID);
  } 
}
