/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Workshop.Mechanic;

import Workshop.CommunicationChannel.ClientComm;
import static Workshop.CommunicationChannel.CommPorts.*;

import Workshop.Messages.GeneralRepositoryMessages;
import Workshop.Messages.LoungeMessages;
import Workshop.Messages.RepairAreaMessages;
import Workshop.Messages.ParkMessages;
/**
 * <p>
 * Mechanic client auxiliary class to send requests to the various interacting servers
 * </p>
 * @author Dinis Canastro
 * @author Bruno Mendes
 */
public class MechanicMethods {
    
    ClientComm gr = new ClientComm(GENERAL_REPOSITORY_NAME,GENERAL_REPOSITORY_PORT);
    ClientComm lg = new ClientComm(LOUNGE_NAME, LOUNGE_PORT);
    ClientComm ra = new ClientComm(REPAIR_AREA_NAME, REPAIR_AREA_PORT);
    ClientComm pk = new ClientComm(PARK_NAME, PARK_PORT);
    
    // ############## Lounge ##################
    /**
    * <p> 
    * [Lounge] 
    * </p>
    */
    public void letManagerKnow(int[] stock_needed, int threadID){
        LoungeMessages response;
        openComm(lg,"Mechanic "+ threadID +": Lounge");
        lg.writeObject(new LoungeMessages(LoungeMessages.LET_MANAGER_KNOW,threadID,stock_needed));
        response = (LoungeMessages) lg.readObject(); 
        lg.close();
    }
     /**
    * <p> 
    * [Lounge] 
    * </p>
    */
    public void repairConcluded(int car, int threadID){
        LoungeMessages response;
        openComm(lg,"Mechanic "+ threadID +": Lounge");
        lg.writeObject(new LoungeMessages(LoungeMessages.REPAIR_CONCLUDED,threadID,car));
        response = (LoungeMessages) lg.readObject(); 
        lg.close();
    }
    
    
    // ############## Repair Area ##################
    /**
    * <p> 
    * [Repair Area] 
    * </p>
    */
    public void readThePaper(int threadID){
        RepairAreaMessages response;
        openComm(ra,"Mechanic "+ threadID +": Repair Area");
        ra.writeObject(new RepairAreaMessages(RepairAreaMessages.READ_THE_PAPER,threadID));
        response = (RepairAreaMessages) ra.readObject(); 
        ra.close();
    
    }
    
    /**
    * <p> 
    * [Repair Area] 
    * </p>
    */
    public int[] startRepairProcedure(int threadID) {
        RepairAreaMessages response;
        openComm(ra,"Mechanic "+ threadID +": Repair Area");
        ra.writeObject(new RepairAreaMessages(RepairAreaMessages.START_REPAIR_PROCEDURE,threadID));
        response = (RepairAreaMessages) ra.readObject(); 
        ra.close();
        return response.getMessageCar();
    }
    
    
    /**
    * <p> 
    * [Repair Area] 
    * </p>
    */
    public  int[] getRequiredPart(int[] car,int threadID) {
        RepairAreaMessages response;
        openComm(ra,"Mechanic "+ threadID +": Repair Area");
        ra.writeObject(new RepairAreaMessages(RepairAreaMessages.GET_REQUIRED_PART,car));
        response = (RepairAreaMessages) ra.readObject(); 
        ra.close();
        return response.getMessageCar();
    }
    
    
     /**
    * <p> 
    * [Repair Area] 
    * </p>
    */
    public int[] partAvailable(int[] car, int threadID) {
        RepairAreaMessages response;
        openComm(ra,"Mechanic "+ threadID +": Repair Area");
        ra.writeObject(new RepairAreaMessages(RepairAreaMessages.PART_AVAILABLE,threadID,car));
        response = (RepairAreaMessages) ra.readObject(); 
        ra.close();
        return response.getMessageCar();
    }
    
     /**
    * <p> 
    * [Repair Area] 
    * </p>
    */
    public void resumeRepairProcedure(int threadID) {
        RepairAreaMessages response;
        openComm(ra,"Mechanic "+ threadID +": Repair Area");
        ra.writeObject(new RepairAreaMessages(RepairAreaMessages.RESUME_REPAIR_PROCEDURE,threadID));
        response = (RepairAreaMessages) ra.readObject(); 
        ra.close();
    }
    
     /**
    * <p> 
    * [Repair Area] 
    * </p>
    */
    public void putCarWaiting(int []car,int threadID) {
        RepairAreaMessages response;
        openComm(ra,"Mechanic "+ threadID +": Repair Area");
        ra.writeObject(new RepairAreaMessages(RepairAreaMessages.PUT_CAR_WAITING,car));
        response = (RepairAreaMessages) ra.readObject(); 
        ra.close();
    }
    
     /**
    * <p> 
    * [Repair Area] 
    * </p>
    */
    public  int[] fixIt(int [] car, int [] part_toRepair,int threadID){
        RepairAreaMessages response;
        openComm(ra,"Mechanic "+ threadID +": Repair Area");
        ra.writeObject(new RepairAreaMessages(RepairAreaMessages.FIX_IT,car,part_toRepair));
        response = (RepairAreaMessages) ra.readObject(); 
        ra.close();
        return response.getMessageCar();
    }
    
     /**
    * <p> 
    * [Repair Area] 
    * </p>
    */
    public void wakeUpPal(int threadID){
        RepairAreaMessages response;
        openComm(ra,"Mechanic "+ threadID +": Repair Area");
        ra.writeObject(new RepairAreaMessages(RepairAreaMessages.WAKE_UP_PAL));
        response = (RepairAreaMessages) ra.readObject(); 
        ra.close();
    
    }
    
    
     /**
    * <p> 
    * [Repair Area] 
    * </p>
    */
    public boolean workdone(int threadID){
        RepairAreaMessages response;
        openComm(ra,"Mechanic "+ threadID +": Repair Area");
        ra.writeObject(new RepairAreaMessages(RepairAreaMessages.DONE, threadID));
        response = (RepairAreaMessages) ra.readObject(); 
        ra.close();
        return response.getMessageResponseBool();
    }
     
    // ############## Park  ##################
    /**
    * <p> 
    * [Park] 
    * </p>
    */
    public boolean getVehicle(int car, int threadID){
        ParkMessages response;
        openComm(pk,"Mechanic "+ threadID +": Park");
        pk.writeObject(new ParkMessages(ParkMessages.GET_VEHICLES,car));
        response = (ParkMessages) pk.readObject(); 
        pk.close();
        return response.getMessageResponseBool();
    }
   
    
    /**
    * <p> 
    * [Park] 
    * </p>
    */
    public boolean returnVehicle(int car,int threadID){
        ParkMessages response;
        openComm(pk,"Mechanic "+ threadID +": Park");
        pk.writeObject(new ParkMessages(ParkMessages.RETURN_VEHICLES,car));
        response = (ParkMessages) pk.readObject(); 
        pk.close();
        return response.getMessageResponseBool();
    }
    
    
    private void openComm(ClientComm cc, String name){
        while(!cc.open()){
            System.out.println(name + " not open, trying again...");
            try{
                Thread.sleep(1000);
            }catch(Exception ex){ }            
        }
    }
     
     
    
    
    
}
