/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Workshop.SupplierSite;

import Workshop.CommunicationChannel.ServerComm;
import Workshop.Messages.SupplierSiteMessages;

/**
 * Instance of Supplier Site Proxy thread that retrieves and processes a single request on a different thread of execution.
 * @author Dinis Canastro
 * @author Bruno Mendes
 */
public class SupplierSiteProxy implements Runnable{
    // Communication
    ServerComm socket;
    SupplierSite ss;
    
    public SupplierSiteProxy(ServerComm socket, SupplierSite ss) {
        this.socket = socket;
        this.ss = ss;
    }
    
    
    @Override
    public void run(){
        
        SupplierSiteMessages msg_received, msg_sent = null;
        
        msg_received = (SupplierSiteMessages) socket.readObject();
        System.out.println("MSG RECEIVED: " + msg_received.getMessageType());
        
        boolean response;
        
        // Processing the message method
        switch (msg_received.getMessageType()) {
            case SupplierSiteMessages.GO_TO_SUPPLIER_SITE:
                response = ss.goToSupplierSite(msg_received.getMessageOrder());
                msg_sent = new SupplierSiteMessages(SupplierSiteMessages.OK, response);
                break;

            case SupplierSiteMessages.END:
                System.out.println("END");
                ss.setDone();
                msg_sent = new SupplierSiteMessages(SupplierSiteMessages.OK);
                socket.writeObject(msg_sent);
                socket.close();
                throw new RuntimeException("Received end message.");
                
            default:
                System.out.println("[ERROR] Received invalid message type: " + msg_received.getMessageType());
                break;
        }
        
        socket.writeObject(msg_sent);
        socket.close();
    }
}
