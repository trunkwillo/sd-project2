/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Workshop.Messages;

import java.io.Serializable;
/**
* Class used to serialize the different types of messages to the Supplier Site Server containing the differnet serializable messages.
* @author Dinis Canastro
* @author Bruno Mendes
*/
public class SupplierSiteMessages implements Serializable {
    
    private int msgType = -1;
    private boolean msgState = false;
    
    public static final int GO_TO_SUPPLIER_SITE = 1;
    
    /**
     * Message to signal the client that his request has been processed with success
     */
    public static final int OK = 20;
    
    /**
     * Message to terminate server activity
     */
    public static final int END = 0;
    
    private int[] order;
    private boolean response;
    
    /**
     * Constructor with the message type (1 integer, 1 integer array)
     * @param msgType type of message received
     * @param order parts to order
     */
    public SupplierSiteMessages(int msgType, int[] order) {
        this.msgType = msgType;
        this.order = order;
    }
    
    /**
     * Constructor with the message type (1 integer, 1 boolean)
     * @param msgType type of message received
     * @param response whether the order was successful or not
     */
    public SupplierSiteMessages(int msgType, boolean response) {
        this.msgType = msgType;
        this.response = response;
    }
     
    /**
     * Constructor with the message type (1 integer)
     * @deprecated
     */
     public SupplierSiteMessages(int msgType){
         this.msgType = msgType;
     }
    
     public int getMessageType(){
        return this.msgType;
    }
    
    
    public int[] getMessageOrder(){
        return this.order;
    }
    
    public boolean getMessageResponse(){
        return this.response;
    }
}