/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Workshop.Messages;

import java.io.Serializable;
/**
* Class used to serialize the different types of messages to the Lounge Server containing the differnet serializable messages.
* @author Dinis Canastro
* @author Bruno Mendes
*/
public class LoungeMessages implements Serializable{
    
    private int msgType = -1;
    private boolean msgState = false;
    
    public static final int APPRAISE_SITUATION = 1;
    public static final int TALK_TO_CUSTOMER = 2;
    public static final int RECEIVE_PAYMENT = 3;
    public static final int HAND_CAR_KEY = 4;
    public static final int QUEUE_IN = 5;
    public static final int TALK_WITH_MANAGER = 6;
    public static final int PAY_FOR_SERVICE = 7;
    public static final int COLLECT_KEY = 8;
    public static final int LET_MANAGER_KNOW = 9;
    public static final int REPAIR_CONCLUDED = 10;
    public static final int GET_NEXT_TASK = 11;
    public static final int NOTIFY_CLIENT = 12;
    public static final int GET_PARTS_ORDERS = 13;
    
    /**
     * Message to signal the client that his request has been processed with success
     */
    public static final int OK = 20;
    
    /**
     * Message to terminate server activity
     */
    public static final int END = 0;
    
    private int threadID;
    private int replacement;
    private int car;
    private int[] stock;
    
    // Responses
    private int response;
    private int[] response_array;
    
    /**
     * Constructor with the message type (1 integer)
     * @param msgType type of message received
     */
    public LoungeMessages(int msgType) {
        this.msgType = msgType;
    }
    
    /**
     * Constructor with the message type (2 integers)
     * @param msgType type of message received
     * @param threadID identification of the sender
     */
    public LoungeMessages(int msgType, int threadID) {
        this.msgType = msgType;
        if(msgType == OK){
            this.response = threadID;
        }else{
            this.threadID = threadID;
        }
       
    }
    
    /**
     * Constructor with the message type (3 integers)
     * @param msgType type of message received
     * @param threadID identification of the sender
     * @param var general use integer
     */
    public LoungeMessages(int msgType, int threadID, int var) {
        this.msgType = msgType;
        this.threadID = threadID;
        if(msgType == REPAIR_CONCLUDED){
            this.car = var;
        }else if(msgType == PAY_FOR_SERVICE){
            this.replacement = var;
        }
        
    }
    
    /**
     * Constructor with the message type (2 integers, 1 integer array)
     * @param msgType type of message received
     * @param threadID identification of the sender
     * @param stock integer array containing stock information
     */
    public LoungeMessages(int msgType, int threadID, int[] stock) {
        this.msgType = msgType;
        this.threadID = threadID;
        this.stock = stock;
    }
    
    /**
     * Constructor with the message type (1 integer, 1 integer array)
     * @param msgType type of message received
     * @param response_array general use integer array
     */
    public LoungeMessages(int msgType, int[] response_array) {
        this.msgType = msgType;
        this.response_array = response_array;
    }
    
    /**
     * Constructor with the message type (1 integers, 1 boolean)
     * @param msgType type of message received
     * @param msgState boolean
     */
    public LoungeMessages(int msgType, boolean msgState) {
        this.msgType = msgType;
        this.msgState = msgState;
    }
    
    
    /**
     * Constructor with the message type (2 integers, 1 boolean)
     * @param msgType type of message received
     * @param msgState boolean value
     * @param response general use integer value
     */
    public LoungeMessages(int msgType, boolean msgState, int response) {
        this.msgType = msgType;
        this.msgState = msgState;
        this.response = response;
    }
    
    
     /**
     * Getter method returning the message type
     * @return integer value representing the message type
     */
    public int getMessageType(){
        return this.msgType;  
    }
    
    /**
     * Getter method returning the state of the message
     * @return boolean value signaling the state of the message
     */
    public boolean getMessageState(){
        return this.msgState;
    }
    
    public int getMessageReplacement(){
        return this.replacement;
    }
    
    public int getMessageThreadID(){
        return this.threadID;
    }
    
    public int getMessageCar(){
        return this.car;
    }
    
    public int[] getMessageStock(){
        return this.stock;
    }
    
    
    // Responses
    
    public int getMessageResponse(){
        return this.response;
    }
    
    public int[] getMessageResponseArray(){
        return this.response_array;
    }
    
    
    
}
