/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Workshop.RepairArea;

import Workshop.CommunicationChannel.ServerComm;
import static Workshop.CommunicationChannel.CommPorts.REPAIR_AREA_PORT;
/**
 * <p>
 * Main class to boot up Repair Area Server.
 * </p>
 * @author Dinis Canastro
 * @author Bruno Mendes
 */
public class RepairAreaMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        RepairArea ra = new RepairArea();
        ServerComm socketCommunication, socketListening;
        Runnable client;

        socketListening = new ServerComm(REPAIR_AREA_PORT);
        socketListening.start();

        System.out.println("[START] Repair Area Server starting");

        boolean done = false;

        /* process requests until END message type is received and processed by the server */
        while (!done) {
            try{
                socketCommunication = socketListening.accept();
                client = new RepairAreaProxy(socketCommunication, ra);
                Thread c = new Thread(client);
                /* handle exception thrown by proxy thread upon END message processing */
                Thread.UncaughtExceptionHandler h = (Thread th, Throwable ex) -> {
                    System.out.println("Repair Area server ended! " + ex);
                    System.exit(0);
                };

                c.setUncaughtExceptionHandler(h);
                c.start(); 

            }
            catch(Exception ex){
                if(ra.getDone()){ // Check if exception is because of end or real exception
                    ra.setDone();
                }else{
                    System.out.print("[EXCEPTION]" + ex);
                }
            }
        } 
    }
    
}
