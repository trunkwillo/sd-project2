/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package CustomerMain.CommunicationChannel;

/**
 *
 * @author diniscanastro
 */
public class CommPorts {    
    public static final int GENERAL_REPOSITORY_PORT = 22440;
    public static final String GENERAL_REPOSITORY_NAME = "l040101-ws01.ua.pt";
    
    public static final int SUPPLIER_SITE_PORT = 22441;
    public static final String SUPPLIER_SITE_NAME = "l040101-ws02.ua.pt";
    
    public static final int OUTSIDE_WORLD_PORT = 22442;
    public static final String OUTSIDE_WORLD_NAME = "l040101-ws03.ua.pt";
    
    public static final int LOUNGE_PORT = 22443;
    public static final String LOUNGE_NAME = "l040101-ws04.ua.pt";
    
    public static final int REPAIR_AREA_PORT = 22444;
    public static final String REPAIR_AREA_NAME = "l040101-ws05.ua.pt";
    
    public static final int PARK_PORT = 22445;
    public static final String PARK_NAME = "l040101-ws06.ua.pt";
    
    public static final int CLIENT_PORT = 22446;
    public static final String CLIENT_NAME = "l040101-ws07.ua.pt";
    
    public static final int MANAGER_PORT = 22447;
    public static final String MANAGER_NAME = "l040101-ws08.ua.pt";
    
    public static final int MECHANIC_PORT = 22448;
    public static final String MECHANIC_NAME = "l040101-ws09.ua.pt";
    
    
    
}
