/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CustomerMain;

import CustomerMain.CommunicationChannel.ClientComm;
import static CustomerMain.CommunicationChannel.CommPorts.*;

import CustomerMain.Messages.GeneralRepositoryMessages;
import CustomerMain.Messages.LoungeMessages;
import CustomerMain.Messages.OutsideWorldMessages;
import CustomerMain.Messages.ParkMessages;
/**
 *
 * @author bmend
 */
public class CustomerMethods {
    // variables
    ClientComm gr = new ClientComm(GENERAL_REPOSITORY_NAME,GENERAL_REPOSITORY_PORT);
    ClientComm lg = new ClientComm(LOUNGE_NAME, LOUNGE_PORT);
    ClientComm ow = new ClientComm(OUTSIDE_WORLD_NAME, OUTSIDE_WORLD_PORT);
    ClientComm pk = new ClientComm(PARK_NAME, PARK_PORT);
    
    
    // ############# General Repository ##################
    public void setReplacementNeeded( String state, int threadID){
        GeneralRepositoryMessages response;
        openComm(gr,"Customer "+ threadID +": General Repository");
        gr.writeObject(new GeneralRepositoryMessages(GeneralRepositoryMessages.SET_REPLACEMENT_NEEDED,state,threadID));
        response = (GeneralRepositoryMessages) gr.readObject(); 
        gr.close();
    }
    
    
    
    // ############## Lounge ##################
    /**
    * <p> 
    * [Lounge] 
    * </p>
    */
    public void queueIn(int threadID){
        LoungeMessages response;
        openComm(lg,"Customer "+ threadID +": Lounge");
        lg.writeObject(new LoungeMessages(LoungeMessages.QUEUE_IN,threadID));
        response = (LoungeMessages) lg.readObject(); 
        lg.close();
    }
    
    /**
    * <p> 
    * [Lounge] 
    * </p>
    */
    public  void talkWithManager(int threadID){
        LoungeMessages response;
        openComm(lg,"Customer "+ threadID +": Lounge");
        lg.writeObject(new LoungeMessages(LoungeMessages.TALK_WITH_MANAGER,threadID));
        response = (LoungeMessages) lg.readObject();
        lg.close();
    }
    
    /**
    * <p> 
    * [Lounge] 
    * </p>
    */
    public void payForTheService(int replacement,int threadID){
        LoungeMessages response;
        openComm(lg,"Customer "+ threadID +": Lounge");
        lg.writeObject(new LoungeMessages(LoungeMessages.PAY_FOR_SERVICE,threadID,replacement));
        response = (LoungeMessages) lg.readObject(); 
        lg.close();
    
    }
    
    
    /**
    * <p> 
    * [Lounge] 
    * </p>
    */
    public  int collectKey(int threadID){
        LoungeMessages response;
        openComm(lg,"Customer "+ threadID +": Lounge");
        lg.writeObject(new LoungeMessages(LoungeMessages.COLLECT_KEY,threadID));
        response = (LoungeMessages) lg.readObject(); 
        lg.close();
        return response.getMessageResponse();
    }
    
    // ############## OutSide World ##################
    
    /**
    * <p> 
    * [OutSide World] 
    * </p>
    */
    public void decideOnRepair(int threadID){
        OutsideWorldMessages response;
        openComm(ow,"Customer "+ threadID +": Outside World");
        ow.writeObject(new OutsideWorldMessages(OutsideWorldMessages.DECIDE_ON_REPAIR));
        response = (OutsideWorldMessages) ow.readObject(); 
        ow.close();
    }
    
    /**
    * <p> 
    * [OutSide World] 
    * </p>
    */
    public void backtoWorkByCar(int threadID){
        OutsideWorldMessages response;
        openComm(ow,"Customer "+ threadID +": Outside World");
        ow.writeObject(new OutsideWorldMessages(OutsideWorldMessages.BACK_TO_WORK_BY_CAR,threadID));
        response = (OutsideWorldMessages) ow.readObject(); 
        ow.close();
    }
    
    /**
    * <p> 
    * [OutSide World] 
    * </p>
    */
    public void backtoWorkByBus(int threadID){
        OutsideWorldMessages response;
        openComm(ow,"Customer "+ threadID +": Outside World");
        ow.writeObject(new OutsideWorldMessages(OutsideWorldMessages.BACK_TO_WORK_BY_BUS,threadID));
        response = (OutsideWorldMessages) ow.readObject(); 
        ow.close();
    
    }
    
    // ############## Park ##################
    
    /**
    * <p> 
    * [Park] 
    * </p>
    */
    public boolean goToRepairShop(int car, int threadID){
        ParkMessages response;
        openComm(pk,"Customer "+ threadID +": Park");
        pk.writeObject(new ParkMessages(ParkMessages.GO_TO_REPAIR_SHOP,threadID,car));
        response = (ParkMessages) pk.readObject(); 
        pk.close();
        return response.getMessageResponseBool();
    }
    
     /**
    * <p> 
    * [Park] 
    * </p>
    */
    public boolean findCar(int car, int threadID) {
        ParkMessages response;
        openComm(pk,"Customer "+ threadID +": Park");
        pk.writeObject(new ParkMessages(ParkMessages.FIND_CAR,threadID,car));
        response = (ParkMessages) pk.readObject(); 
        pk.close();
        return response.getMessageState();
    }
    
     /**
    * <p> 
    * [Park] 
    * </p>
    */
    public int collectCar(int threadID){
        ParkMessages response;
        openComm(pk,"Customer "+ threadID +": Park");
        pk.writeObject(new ParkMessages(ParkMessages.COLLECT_CAR,threadID));
        response = (ParkMessages) pk.readObject(); 
        pk.close();
        return response.getMessageResponseInt();
    }
    
     /**
     * Generic method to open the communication to the server.
     * In case of failure keeps trying to initiate communication in 1 second intervals
     * 
     * @param cc the communication channel to open the connection
     * @param name Name of the monitor corresponding to the channel
     */
    private void openComm(ClientComm cc, String name){
        while(!cc.open()){
            System.out.println(name + " not open, trying again...");
            try{
                Thread.sleep(1000);
            }catch(Exception ex){ }            
        }
    }
    
}
