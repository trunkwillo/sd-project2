/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CustomerMain.Monitors;

import CustomerMain.Interfaces.CustomerOutsideWorldInterface;
import CustomerMain.Interfaces.ManagerOutsideWorldInterface;
import genclass.GenericIO;
import java.util.*;


public class OutsideWorld implements CustomerOutsideWorldInterface, ManagerOutsideWorldInterface {
    private boolean [] carReadyTogo;
    private int customer_number;
    private GeneralRepository gr;
    


    public OutsideWorld(int customer_number, GeneralRepository gr){
        this.carReadyTogo = new boolean[customer_number];
        Arrays.fill(carReadyTogo,Boolean.FALSE);
        this.customer_number = customer_number;
        this.gr = gr;
    }

    
    /**
    * <p> Customer waits until he's ready to go to repair shop.</p>
    * 
    */
    @Override
    public synchronized void decideOnRepair(){
        while(!probability()); 
    }

   
    /**
    * <p> Customer is in the Outside World waiting for his car to be ready,we can have two situations:</p>
    * <p> 1 - Customer already talked with the Manager and he have a replacemente car, waiting for his car to be ready. </p>
    * <p> 2 - His car is ready to go and continue with his life until dying . </p>
    * @param thread_id used to see if is their own car. 
    */
    @Override
    public synchronized void backtoWorkByCar(int thread_id){
        gr.setCustomerState("NLC", thread_id);
        while(!carReadyTogo[thread_id]){ 
            try{
                wait();
            }catch(InterruptedException ex){
                Thread.currentThread().interrupt();
            }
        }
    }


 
    /**
    * <p> Car is ready to go, and Manager call to Customer to alerting him. </p>
    * 
    * @param car_owner Id of ther car owner to notify him. 
    */
    @Override
    public synchronized void phoneCustomer(int car_owner ){
        //System.out.println("Client with threadID " + car_owner + " your car is ready to go.");
        gr.setManagerState("ALC");
        carReadyTogo[car_owner] = true;
        notifyAll(); //manager acorda os clientes !!
    }

 
    /**
    * <p> Customer is in  the Outside World waiting for the Manager to notifi him. </p>
    * @param thread_id used to see if is their own car. 
    */
    @Override
    public synchronized void backtoWorkByBus(int thread_id){
        //fica aqui à espera de um aviso do manager para saber se o carro está pronto!
        gr.setCustomerCar("-", thread_id);
        gr.setCustomerState("NLO", thread_id);
        while(!carReadyTogo[thread_id]){ 
            try{
                wait();
            }catch(InterruptedException ex){
                Thread.currentThread().interrupt();
            }
        }
    }

// ------------------------------------------------------------
// ----------------- AUX FUNCS --------------------------------
    /**
    * <p> This function is used to calculated the probability of one client to go to the repair shop.</p>
    *  
    */
    private boolean probability(){
        Random gerador = new Random();
        Double prob = 0.0;
        prob = gerador.nextDouble();
        if(prob > 0.8){
            return true;
        }
        else{
            return false;
        }
    }
}
