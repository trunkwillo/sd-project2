/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CustomerMain.Monitors;

import genclass.FileOp;
import genclass.GenericIO;
import genclass.TextFile;
import java.util.*;
/**
 *
 * @author bmend
 */
public class GeneralRepository {
    // File
    
    // States
    
    private String manager_state = "CWT";
    private String[] mechanic_state = new String[2];
    private String[] customer_state = new String[30];
    
    // Customer info
    private String[] car_driven = new String[30 + 3];           // ID of the driven car | Customer's - "Customer ID" / Replacement - "R0x" / None - "-"
    private String[] replacement_needed = new String[30];   // Customer requires/required replacement car | (T)rue or (F)alse
    private String[] repair_done = new String[30];          // Customer car is already fixed | (T)rue or (F)alse
    
    // Monitor states
    // Lounge
    private int entrance_qq = 0;
    private int replacement_qq = 0;
    private int cars_repaired_count = 0;
    
    // Park
    private int customer_cars_parked = 0;
    private int replacement_cars_parked = 3;
    
    // Repair Area
    private int requests_by_manager = 0;                            // Counter of the number of requests made by the manager
    private int[] part_stock = new int[3];                          // Number of part x in stock
    private int[] part_required = new int[3];                       // Number of part x required to resume the fixes
    private String[] manager_notified = new String[3];              // Part X missing notification sent | (T)rue or (F)alse
    
    // Supplier site
    private int[] parts_ordered = new int[3];               // Counting part x number of orders completed
    
//    String output = String.format("%s  %s %s  %s %s %s %s  %s %s %s %s  %s %s %s %s  %s %s %s %s  %s %s %s %s  %s %s %s %s  %s %s %s %s  %s %s %s %s  %s %s %s %s  %s %s %s %s\n"
//                                + "           %s %s %s %s  %s %s %s %s  %s %s %s %s  %s %s %s %s  %s %s %s %s  %s %s %s %s  %s %s %s %s  %s %s %s %s  %s %s %s %s  %s %s %s %s\n"
//                                + "           %s %s %s %s  %s %s %s %s  %s %s %s %s  %s %s %s %s  %s %s %s %s  %s %s %s %s  %s %s %s %s  %s %s %s %s  %s %s %s %s  %s %s %s %s\n"
//                                + "             %d %d %d    %d %d   %d %d %d %s    %d %d %s    %d %d %s   %d %d %d",
//                                manager_state, mechanic_state[0], mechanic_state[1], customer_state[0]...);
    
    // Ficheiro
    String fileName = "final_log.txt"; 
    TextFile log = new TextFile();
    
    public GeneralRepository(){
        if(FileOp.exists(".", fileName)){
            FileOp.deleteFile(".",fileName);
        }
        
        // Initialization
        Arrays.fill(mechanic_state, "RTN");
        Arrays.fill(customer_state, "NLC");
        
        for(int i = 0; i < car_driven.length; i++){
            car_driven[i] = Integer.toString(i);
        }
        Arrays.fill(replacement_needed, "F");
        Arrays.fill(repair_done, "F");
        
        Arrays.fill(part_stock, 10);
        Arrays.fill(part_required, 0);
        Arrays.fill(manager_notified, "F");
        
        Arrays.fill(parts_ordered, 0);
    }

    
    /**
     * <p> Generates the logger with a multitude of information.</p>
     * <p> The following states are conceived for each entity:</p>
        <p>Manager States:</p>
            <p>- Checking What To do = CWT</p>
            <p>- Getting New Parts = GNP</p>
            <p>- RePlenish Stock = RPS</p>
            <p>- ALerting Customer = ALC</p>
            <p>- ATtending Customer = ATC</p>
            <p>- PoSting Job = PSJ</p>
    
        <p>Mechanic States:</p>
            <p>- Waiting For Work = WFW</p>
            <p>- Fixing The Car = FTC</p>
            <p>- Checking STock = CST</p>
            <p>- ALerting Manager = ALM</p>
    
        <p>Customer States:</p>
            <p>- Normal Life with Car = NLC</p>
            <p>- Normal Life withOut car = NLO</p>
            <p>- PaRK = PRK</p>
            <p>- ReCePtion = RCP</p>
            <p>- Waiting for Replace Car =  WRC</p>
    */
    public void generateLog(){
        if (!log.openForAppending(".", fileName)){ 
            GenericIO.writelnString("A operação de criação do ficheiro " + fileName + " falhou!");
            System.exit(1);
        }
        
        // Manager state
        String msg = "" + manager_state;
        // Mechanics state
        msg = msg + String.format("\t%s %s\t", mechanic_state[0], mechanic_state[1]);
        // Customers state
        for(int i = 0; i < customer_state.length; i++){
            String temp;
            if(car_driven[i].equals("30")){
                temp = "R0";
            }else if(car_driven[i].equals("31")){
                temp = "R1";
            }else if(car_driven[i].equals("32")){
                temp = "R2";
            }else{
                temp = car_driven[i];
            }
            msg = msg + String.format("\t%s %s %s %s", customer_state[i], temp, replacement_needed[i], repair_done[i]);
            if((i + 1) % 10 == 0){ // Next line selection
                msg = msg + "\n\t\t";
            }
        }
        // Lounge data
        msg = msg + String.format("\t%d %d %d", entrance_qq, replacement_qq, cars_repaired_count);
        // Park data
        msg = msg + String.format("\t%d %d", customer_cars_parked, replacement_cars_parked);
        // Repair Area data
        msg = msg + String.format("\t%d", requests_by_manager);
        for(int i = 0; i < part_stock.length; i++){
            msg = msg + String.format("\t%d %d %s\t", part_stock[i], part_required[i], manager_notified[i]);
        }
        // Supplier Site data
        for(int i = 0; i < parts_ordered.length; i++){
            msg = msg + String.format(" %d", parts_ordered[i]);
        } 
        msg = msg + "\n";
        //msg = msg + "\n-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------";
        // Write to file
        log.writelnString(msg);
        if (!log.close()){ 
            GenericIO.writelnString("A operação de fecho do ficheiro " + fileName + " falhou!");
            System.exit(1);
        }
    }
    
    // Setters for states
    public synchronized void setManagerState(String state){
        manager_state = state;
        generateLog();
    }
    
    public synchronized void setCustomerState(String state, int threadID){
        customer_state[threadID] = state;
        generateLog();
    }
    
    public synchronized void setMechanicState(String state, int threadID){
        mechanic_state[threadID - 30] = state;
        generateLog();
    }
    
    
    // Setters for customer info
    public synchronized void setCustomerCar(String car, int threadID){
        car_driven[threadID] = car;
        generateLog();
    }
    
    public synchronized void setReplacementNeeded(String car, int threadID){
        replacement_needed[threadID] = car;
        //generateLog();
    }
    
    public synchronized void setRepairDone(String car, int threadID){
        repair_done[threadID] = car;
        generateLog();
    }
    
    
    // Setters for monitors
        // Lounge
    public synchronized void setEntranceQueue(int size){
        entrance_qq = size;
        generateLog();
    }
    
    public synchronized void setReplacementQueue(int size){
        replacement_qq = size;
        generateLog();
    }
    
    public synchronized void increaseRepairedCount(){
        cars_repaired_count++;
        generateLog();
    }
    
        // Park
    public synchronized void increaseCustomerParked(){
        customer_cars_parked++;
        generateLog();
    }
    
    public synchronized void decreaseCustomerParked(){
        customer_cars_parked--;
        generateLog();
    }
    
    public synchronized void increaseReplacementParked(){
        replacement_cars_parked++;
        generateLog();
    }
    public synchronized void decreaseReplacementParked(){
        replacement_cars_parked--;
        generateLog();
    }
    
    
        // Repair Area  
    public synchronized void increaseManagerRequests(){
        requests_by_manager++;
        generateLog();
    }
    
    public synchronized void setPartStock(int[] parts){
        part_stock = parts;
        generateLog();
    }
    
    public synchronized void setPartRequirements(int[] parts){
        part_required = parts;
        generateLog();
    }
    
    public synchronized void setNotifications(String[] flags){
        manager_notified = flags;
        generateLog();
    }
    
        // Supplier site
    public synchronized void setOrdersFilled(int[] orders){
        for(int i = 0; i < parts_ordered.length; i++){
            parts_ordered[i] += orders[i];
        }
        generateLog();
    }
    
}
