/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CustomerMain.Entities;

//import WorkShop.AuxClasses.Car;
import CustomerMain.Monitors.GeneralRepository;
import CustomerMain.Monitors.Lounge;
import CustomerMain.Monitors.OutsideWorld;
import CustomerMain.Monitors.Park;
import CustomerMain.Monitors.RepairArea;
import CustomerMain.Monitors.SupplierSite;

import genclass.GenericIO;
import java.util.*;
/**
 *
 * @author bmend
 */
public class Manager implements Runnable {
    private int threadID;
    GeneralRepository gr;
    Lounge lg;
    OutsideWorld ow;
    RepairArea ra;
    SupplierSite ss;
    Park pk;

    int post_it_description;
    int post_it_info;
    
    int customer_count = 0;

    public Manager(int threadID, GeneralRepository gr, Lounge lg, OutsideWorld ow, RepairArea ra, SupplierSite ss, Park pk){
      this.threadID = threadID;
      this.gr = gr;
      this.lg = lg;
      this.ow = ow;
      this.ra = ra;
      this.ss = ss;
      this.pk = pk;
    }
    
    @Override
    public void run(){
        while(customer_count < 30){
            lg.getNextTask();
            switch(lg.appraiseSit()){
                case 1: // Talk with customers
                    //gr.setManagerState("ATC");
                    int decision = lg.talkToCustomer();
                    if(decision >= 0 && decision <= 29){          // ID of car to repair
                        ra.registerService(decision);
                        //gr.setManagerState("PSJ");
                    }else if(decision == -1){   // Receive Payment
                        lg.receivePayment();
                        customer_count++;
                    }
                    break;
                case 2: // Stock to refill 
                    int[] order = lg.getPartsOrders();
                    // Get more parts than the requireds
                    order = addExtra(order);
                    if(!ss.goToSupplierSite(order)){
                        throw new RuntimeException("Suplier site failed horribly!");
                    }
                    //gr.setManagerState("GNP");
                    ra.storePart(order);
                    //gr.setManagerState("RPS");
                    break;
                case 3: // Customers to notify
                    int tmp = lg.notifyClient();
                    if(tmp == -1){
                        break;
                    }
                    ow.phoneCustomer(tmp);
                    //gr.setManagerState("ALC");
                    break;
                case 4: // Hand car keys 
                    //gr.setManagerState("ATC");
                    lg.handCarKey();
                    break;
                default:
                    throw new RuntimeException("Task with wrong or undefined description");
            }
            //gr.setManagerState("CWD");
        }
    }
    
    // ---------------------- Auxiliary Methods -------------------------
    
    /**
    * <p> 
    *   Increases the amount of parts on the order.
    *   This decreases the amount of trips made to the supplier site.
    * </p>
    * @param order has information relative to the quantity required of each part.
    * @return Returns the current required order list increased by the extra amount.
    */
        private int[] addExtra(int[] order){
            for(int i = 0; i < order.length; i++){
                order[i] += 5;
            }
            return order;
        }
}
