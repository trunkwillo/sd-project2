/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CustomerMain.Entities;

import CustomerMain.Monitors.GeneralRepository;
import CustomerMain.Monitors.Lounge;
import CustomerMain.Monitors.Park;
import CustomerMain.Monitors.RepairArea;

import genclass.GenericIO;
import java.util.*;
/**
 *
 * @author bmend
 */
public class Mechanic implements Runnable{
  int threadID;
  GeneralRepository gr;
  Lounge lg;
  RepairArea ra;
  Park pk;
  int nparts = 3;
  int[] car = new int[1+nparts];
  int[] part_toRepair = new int[nparts];
  
  

  public Mechanic(int threadID, GeneralRepository gr, Lounge lg, RepairArea ra, Park pk){
    this.threadID = threadID;
    this.gr = gr;
    this.lg = lg;
    this.ra = ra;
    this.pk = pk;
  }

  @Override
  public void run(){
    while(ra.done()){
       ra.readThePaper(threadID);
       this.car = ra.startRepairProcedure(threadID);
       if(this.car[0] == -1){
           continue;
       }
       //gr.setMechanicState("FTC", threadID);
       pk.getVehicle(this.car[0]);
       int[] order = ra.partAvailable(this.car, threadID);
       //gr.setMechanicState("CST", threadID);
       if(order[0] == -1){
         part_toRepair = ra.getRequiredPart(this.car);
         ra.resumeRepairProcedure(threadID);
         //gr.setMechanicState("FTC", threadID);
         this.car = ra.fixIt(this.car, this.part_toRepair);
         pk.returnVehicle(this.car[0]);
         lg.repairConcluded(this.car[0], threadID);
         //gr.setMechanicState("ALM", threadID);
       }
       else{
         ra.putCarWaiting(this.car);
         lg.letManagerKnow(order, threadID); 
         //gr.setMechanicState("ALM", threadID);
       }
       //gr.setMechanicState("WFW", threadID);
    }
    ra.wakeUpPal();
  } 
}
