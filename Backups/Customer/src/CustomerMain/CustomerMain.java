/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CustomerMain;

/**
 *
 * @author bmend
 */
public class CustomerMain  {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int customer_number = 30;
        
        
        Thread[] threads = new Thread[customer_number];
        
        for(int i = 0; i < customer_number ; i++){
            System.out.println("\u001B[32m[INIT]\u001B[0m CUSTOMER with thread " + i + " started.");
            Runnable c_runnable = new Customer(i);
            threads[i] = new Thread(c_runnable);
            threads[i].start();
        }
        for(int i = 0; i < threads.length; i++){
            try{
                threads[i].join();
            } catch (InterruptedException ex) {
                System.out.println("Deu ruim!"+ex.toString());
                System.exit(1);
            }   
        }
        
    }

  
    
}
