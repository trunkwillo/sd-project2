/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CustomerMain;

import CustomerMain.CommunicationChannel.ClientComm;
import static CustomerMain.CommunicationChannel.CommPorts.*;
import java.util.Random;
/**
 *
 * @author bmend
 */
public class Customer implements Runnable {
    private int threadID;
    private ClientComm gr ;
    private ClientComm lg;
    private ClientComm ow;
    private ClientComm pk;
    private int c;
    private boolean wantRepCar;
    private boolean hasRepCar;
    private int rep_car= -1;
    private int c_temp;
    private CustomerMethods cc = new CustomerMethods();
    
    public Customer(int threadID ){
        this.threadID = threadID;
        this.gr = new ClientComm(GENERAL_REPOSITORY_NAME,GENERAL_REPOSITORY_PORT);
        this.lg = new ClientComm(LOUNGE_NAME, LOUNGE_PORT);
        this.ow = new ClientComm(OUTSIDE_WORLD_NAME, OUTSIDE_WORLD_PORT);
        this.pk = new ClientComm(PARK_NAME, PARK_PORT);
        this.c = threadID;
        this.wantRepCar = repCar();
    }
    
    
    @Override
    public void run(){
        if(wantRepCar) cc.setReplacementNeeded("T", threadID);
        cc.decideOnRepair(threadID); // wait
        cc.goToRepairShop(threadID, threadID);
        //gr.setCustomerState("PRK", threadID);
        cc.queueIn(threadID);//wait
        //gr.setCustomerState("RCP", threadID);
        cc.talkWithManager(threadID); 
          if(this.wantRepCar){
            rep_car = cc.collectKey(threadID);
            //gr.setCustomerCar(String.valueOf(rep_car), threadID);
            //gr.setCustomerState("WRC", threadID);
            cc.findCar(rep_car, threadID);
            //gr.setCustomerState("PRK", threadID);
            cc.backtoWorkByCar(threadID);
            //gr.setCustomerState("NLC", threadID);
            cc.goToRepairShop(rep_car, threadID);
            //gr.setCustomerState("PRK", threadID);
        }else{
            cc.backtoWorkByBus(threadID);
            //gr.setCustomerCar("-", threadID);
            //gr.setCustomerState("NLO", threadID);
        }
        //gr.setRepairDone("T", threadID);
        cc.queueIn(threadID);
        //gr.setCustomerState("RCP", threadID);
        cc.payForTheService(rep_car,threadID);
        c_temp = cc.collectCar(threadID);
        //gr.setCustomerCar(String.valueOf(threadID), threadID);
        //gr.setCustomerState("PRK", threadID);
        if (c != c_temp )  {
            throw new RuntimeException("Returned Wrong Car!");
        }
        cc.backtoWorkByCar(threadID);
        //gr.setCustomerState("NLC", threadID);
    }


// ------------------------------------------------------------
// ----------------- AUX FUNCS --------------------------------

    /**
    * <p> Calculate  the probability of one Costumer ask for 
    * a replace car. </p> 
    *@return True or False to have or not replacement car
    */
    public boolean repCar(){
        return probability();
    }

    /**
    * <p> If are substituion cars to give to the client, this flag is put at True.</p> 
    */
    public void set_hasRepCar(){
        this.hasRepCar = true;
    }



    /**
    * <p> Used to verigy if Costumer has replacement car or not.</p> 
    */
    public boolean get_hasRepCar(){
        return this.hasRepCar;
    }

    /**
    * <p> Small fuction used to calculate the probability of the client goes to talk with the manageror not.</p> 
    *@return True if the probability is higher them 80%, then the Customer goes to talk with the manager.
    */
    private boolean probability(){
        Random gerador = new Random();
        Double prob = 0.0;
        prob = gerador.nextDouble();
        if(prob > 0.8){
            return true;
        }
        else{
            return false;
        }
    }
}

    
    

