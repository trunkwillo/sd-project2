/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package CustomerMain.Messages;

import java.io.Serializable;

/**
 *
 * @author diniscanastro
 */
public class RepairAreaMessages implements Serializable{
     private int msgType = -1;
     private boolean msgState = false;

     public static final int READ_THE_PAPER = 1;
     public static final int START_REPAIR_PROCEDURE = 2;
     public static final int GET_REQUIRED_PART = 3;
     public static final int PART_AVAILABLE = 4;
     public static final int RESUME_REPAIR_PROCEDURE = 5;
     public static final int PUT_CAR_WAITING = 6;
     public static final int FIX_IT = 7;
     public static final int STORE_PART = 8;
     public static final int REGISTER_SERVICE = 9;
     public static final int WAKE_UP_PAL = 10;
     public static final int DONE = 11;




     /**
      * Message to signal the client that his request has been processed with success
      */
     public static final int OK = 20;

     /**
      * Message to terminate server activity
      */
     public static final int END = 0;

     private int threadID;
     private int[] response;
     private int[] car;
     private int[] part_to_repair;
     private int[] new_parts;
     private int carID;
     private boolean responseBool;


     public RepairAreaMessages(int msgType) {
         this.msgType = msgType;
     }

     public RepairAreaMessages(int msgType,int var){
         this.msgType = msgType;
         if(msgType == REGISTER_SERVICE){
             this.carID = var;
         }else{
             this.threadID = var;
         }
     }

     public RepairAreaMessages(int msgType,int threadID, int[] car) {
         this.msgType = msgType;
         this.threadID = threadID;
         this.car = car;
     }

     public RepairAreaMessages(int msgType, int[] var) {
         this.msgType = msgType;
         this.threadID = threadID;
         if(msgType == STORE_PART){
             this.new_parts = var;
         }else{
             this.car = var;
         }
     }

     public RepairAreaMessages(int msgType, int[] car, int[] part_to_repair) {
         this.msgType = msgType;
         this.car = car;
         this.part_to_repair = part_to_repair;
     }


     /***
      *
      * @param msgType
      * @param msgState
      * Response
      */
     public RepairAreaMessages(int msgType, boolean responseBool) {
         this.msgType = msgType;
         this.responseBool = responseBool;
     }

     public RepairAreaMessages(int msgType, boolean msgState, int[] response) {
         this.msgType = msgType;
         this.msgState = msgState;
         this.response = response;
     }


      /**
      * Getter method returning the message type
      * @return integer value representing the message type
      */
     public int getMessageType(){
         return this.msgType;
     }

     public boolean getMessageResponseBool(){
         return this.responseBool;
     }

     public int getMessageThreadID(){
         return this.threadID;
     }

     public int[] getMessageResponse(){
         return this.response;
     }

     public int[] getMessageCar(){
         return this.car;
     }

     public int[] getMessagePartToRepair(){
         return this.part_to_repair;
     }

     public int[] getMessageNewParts(){
         return this.new_parts;
     }

     public int getMessageCarID(){
         return this.carID;
     }
 }
