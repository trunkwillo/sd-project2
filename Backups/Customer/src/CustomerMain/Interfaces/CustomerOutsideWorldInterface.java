/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package CustomerMain.Interfaces;

/**
 *
 * @author diniscanastro
 */
public interface CustomerOutsideWorldInterface {

    /**
     * <p> Customer is in  the Outside World waiting for the Manager to notifi him. </p>
     * @param thread_id used to see if is their own car.
     */
    void backtoWorkByBus(int thread_id);

    /**
     * <p> Customer is in the Outside World waiting for his car to be ready,we can have two situations:</p>
     * <p> 1 - Customer already talked with the Manager and he have a replacemente car, waiting for his car to be ready. </p>
     * <p> 2 - His car is ready to go and continue with his life until dying . </p>
     * @param thread_id used to see if is their own car.
     */
    void backtoWorkByCar(int thread_id);

    /**
     * <p> Customer waits until he's ready to go to repair shop.</p>
     *
     */
    void decideOnRepair();
    
}
