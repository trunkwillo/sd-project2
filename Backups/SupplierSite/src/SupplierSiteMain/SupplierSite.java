/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package SupplierSiteMain;

import static ManagerMain.CommunicationChannel.CommPorts.GENERAL_REPOSITORY_NAME;
import static ManagerMain.CommunicationChannel.CommPorts.GENERAL_REPOSITORY_PORT;
import SupplierSiteMain.CommunicationChannel.ClientComm;
import SupplierSiteMain.Interfaces.ManagerSupplierSiteInterface;
import SupplierSiteMain.Messages.GeneralRepositoryMessages;

/**
 *
 * @author diniscanastro
 */
public class SupplierSite implements ManagerSupplierSiteInterface{
    ClientComm gr;
    
    private boolean done = false;
    
    public SupplierSite(){
        this.gr = new ClientComm(GENERAL_REPOSITORY_NAME, GENERAL_REPOSITORY_PORT);
    }
    
    @Override
    public synchronized boolean goToSupplierSite(int[] order){
        setManagerState("GNP");
        setOrdersFilled(order);
        return true;
    }
    
    public synchronized void setDone(){
        this.done = true;
    }
    
    public synchronized boolean getDone(){
        return this.done;
    }
    
    // General Repository methods
    
    public synchronized void setManagerState(String t){
        GeneralRepositoryMessages response;
        openComm(gr,"Lounge General Repository Update method.");
        gr.writeObject(new GeneralRepositoryMessages(GeneralRepositoryMessages.SET_MANAGER_STATE, t));
        response = (GeneralRepositoryMessages) gr.readObject(); 
        gr.close();
    }
    
    public synchronized void setOrdersFilled(int[] order){
        GeneralRepositoryMessages response;
        openComm(gr,"Lounge General Repository Update method.");
        gr.writeObject(new GeneralRepositoryMessages(GeneralRepositoryMessages.SET_MANAGER_STATE, order));
        response = (GeneralRepositoryMessages) gr.readObject(); 
        gr.close();
    }
    
    private void openComm(ClientComm cc, String name){
        while(!cc.open()){
            System.out.println(name + " not open, trying again...");
            try{
                Thread.sleep(1000);
            }catch(Exception ex){ }            
        }
    }
}
