/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SupplierSiteMain;

import static ManagerMain.CommunicationChannel.CommPorts.LOUNGE_PORT;
import SupplierSiteMain.CommunicationChannel.ServerComm;

/**
 *
 * @author bmend
 */
public class SupplierSiteMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        SupplierSite ss = new SupplierSite();
        ServerComm socketCommunication, socketListening;
        Runnable client;
        
        socketListening = new ServerComm(LOUNGE_PORT);
        socketListening.start();
        
        System.out.println("[START] Supplier Site Server starting");
        
        boolean done = false;
        
        /* process requests until END message type is received and processed by the server */
        while (!done) {
            try{
                socketCommunication = socketListening.accept();
                client = new SupplierSiteProxy(socketCommunication, ss);
                Thread c = new Thread(client);
                /* handle exception thrown by proxy thread upon END message processing */
                Thread.UncaughtExceptionHandler h = (Thread th, Throwable ex) -> {
                    System.out.println("Supplier Site server ended!");
                    System.exit(0);
                };
                
                c.setUncaughtExceptionHandler(h);
                c.start(); 
                
            }
            catch(Exception ex){
                if(ss.getDone()){ // Check if exception is because of end or real exception
                    done = true;
                }else{
                    System.out.print("[EXCEPTION]" + ex);
                }
            }
        } 
    }    
}
    
