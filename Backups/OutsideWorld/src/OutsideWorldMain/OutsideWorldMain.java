/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OutsideWorldMain;

import OutsideWorldMain.CommunicationChannel.ClientComm;
import static OutsideWorldMain.CommunicationChannel.CommPorts.*;
import OutsideWorldMain.CommunicationChannel.ServerComm;
/**
 *
 * @author bmend
 */
public class OutsideWorldMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        OutsideWorld ow = new OutsideWorld(30);
        ServerComm socketCommunication, socketListening;
        Runnable owp;
        
        socketListening = new ServerComm(OUTSIDE_WORLD_PORT);
      
        socketListening.start();
        
        
        System.out.println("[START] Outside world Server starting");
        
        boolean done = false;
        
        /* process requests until END message type is received and processed by the server */
        while (!done) {
            try{
                socketCommunication = socketListening.accept();
                owp = new OutsideWorldProxy(socketCommunication,ow);
                Thread c = new Thread(owp);
                /* handle exception thrown by proxy thread upon END message processing */
                Thread.UncaughtExceptionHandler h = (Thread th, Throwable ex) -> {
                    System.out.println("Outside world server is done for today!");
                    System.exit(0);
                };
                
                c.setUncaughtExceptionHandler(h);
                c.start(); 
                
            }
            catch(Exception ex){
                if(!ow.getDone()){
                    done = true;
                }else{
                    System.out.print(ex);
                }
            }
        }
        
        System.out.println("Betting center server ended");
        
    }
    
}