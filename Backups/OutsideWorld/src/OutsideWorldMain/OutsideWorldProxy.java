/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OutsideWorldMain;

import OutsideWorldMain.CommunicationChannel.ServerComm;
import OutsideWorldMain.Messages.OutsideWorldMessages;

/**
 *
 * @author bmend
 */
public class OutsideWorldProxy implements Runnable{

    ServerComm cc;
    OutsideWorld ow ;
    /**
     * Outside World  instantiation
     * 
     * @param socketCommunication Communication channel
     * 
     */
    public OutsideWorldProxy(ServerComm socketCommunication, OutsideWorld ow ) {
        this.cc = socketCommunication;
        this.ow = ow;
    }

    

    /**
     * Life cycle
     */
    @Override
    public void run() {
        OutsideWorldMessages inMsg,outMsg = null;
        
        
        inMsg = (OutsideWorldMessages) cc.readObject();
        
        
        switch (inMsg.getMessageType()) {
            case OutsideWorldMessages.DECIDE_ON_REPAIR :
                ow.decideOnRepair();
                inMsg = new OutsideWorldMessages(OutsideWorldMessages.OK);
                break;
                
            case OutsideWorldMessages.BACK_TO_WORK_BY_CAR :
                ow.backtoWorkByCar(inMsg.getMessageThreadID());
                inMsg = new OutsideWorldMessages(OutsideWorldMessages.OK);
                break;
                
            case OutsideWorldMessages.BACK_TO_WORK_BY_BUS :
                ow.backtoWorkByBus(inMsg.getMessageThreadID());
                inMsg = new OutsideWorldMessages(OutsideWorldMessages.OK);
                break;
         
            case OutsideWorldMessages.PHONE_CUSTOMER :
                ow.phoneCustomer(inMsg.getMessageOwner());
                inMsg = new OutsideWorldMessages(OutsideWorldMessages.OK);
                break;

            case OutsideWorldMessages.END:
               System.out.println("END");
               ow.setDone();
               inMsg = new OutsideWorldMessages(OutsideWorldMessages.OK);
               throw new RuntimeException("Received end message.");
                 
                 
            default:
                System.out.println("[ERROR] Received invalid message type: " + inMsg);
                break;
        
        }
        
        
    }
    

}
