/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package OutsideWorldMain.Messages;

import java.io.Serializable;

/**
 *
 * @author diniscanastro
 */
public class OutsideWorldMessages implements Serializable{
    
    private int msgType = -1;
    private boolean msgState = false;
    
    public static final int DECIDE_ON_REPAIR = 1;
    public static final int BACK_TO_WORK_BY_CAR = 2;
    public static final int PHONE_CUSTOMER = 3;
    public static final int BACK_TO_WORK_BY_BUS = 4;
    
    
    /**
     * Message to signal the client that his request has been processed with success
     */
    public static final int OK = 20;
    
    /**
     * Message to terminate server activity
     */
    public static final int END = 0;
    
    private int threadID;
    private int owner;
    
    /**
     * Constructor with the message type
     * @param msgType type of message received
     */
    public OutsideWorldMessages(int msgType) {
        this.msgType = msgType;
    }
    
    /**
     * Constructor with the message type
     * @param msgType type of message received
     */
    public OutsideWorldMessages(int msgType,int var) { 
        this.msgType = msgType;
        if(msgType == PHONE_CUSTOMER){
            this.owner = var;
        }else{
            this.threadID = var;
        }
    }
    
    /***
     * 
     * @param msgType
     * @param msgState 
     * Response
     */
    public OutsideWorldMessages(int msgType, boolean msgState) {
        this.msgType = msgType;
        this.msgState = msgState;
    }
    
    
     /**
     * Getter method returning the message type
     * @return integer value representing the message type
     */
    public int getMessageType(){
        return this.msgType;  
    }
    
    /**
     * Getter method returning the state of the message
     * @return boolean value signaling the state of the message
     */
    public boolean getMessageState(){
        return this.msgState;
    }
    
    public int getMessageThreadID(){
        return this.threadID;
    } 
        
    public int getMessageOwner(){
        return this.owner;
    }
     
}
