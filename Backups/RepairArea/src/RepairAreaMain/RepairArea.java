/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RepairAreaMain;

/**
 *
 * @author bmend
 */

import RepairAreaMain.CommunicationChannel.ClientComm;
import static RepairAreaMain.CommunicationChannel.CommPorts.GENERAL_REPOSITORY_NAME;
import static RepairAreaMain.CommunicationChannel.CommPorts.GENERAL_REPOSITORY_PORT;
import RepairAreaMain.Interfaces.MechanicRepairAreaInterface;
import RepairAreaMain.Messages.GeneralRepositoryMessages;

import genclass.GenericIO;
import java.util.*;


public class RepairArea implements MechanicRepairAreaInterface {
    private Queue<int[]> fix_list = new LinkedList<>();
    private Queue<int[]> suspended = new LinkedList<>();
    private int [] stock = new int[3];
    private int nparts = 3;
    private int carCount = 0;
    private boolean orderFill = false;
    private boolean end_ofDay = false;
    private boolean done = false;
    // Monitors
    ClientComm gr;
    
    private int initial_stock = 10;

    public RepairArea(){
      for(int i = 0; i < this.stock.length;i++){
        this.stock[i] = initial_stock;
      }
      this.gr = new ClientComm(GENERAL_REPOSITORY_NAME, GENERAL_REPOSITORY_PORT);
    }

    
    /**
    * <p> Mechanic in this function stay waiting for something to do, 
    *  he is waiting to be waked by the Manager. </p> 
    */
    @Override
    public synchronized void readThePaper(int threadID){
        setMechanicState("WFW", threadID);
        while(fix_list.isEmpty() && (!orderFill || suspended.isEmpty()) && !end_ofDay){ 
            try{
                wait();
            }catch(InterruptedException ex){
                Thread.currentThread().interrupt();
            }
        }
    }
    
   
    /**
    * <p> Mechanic now have a car to repair so he start the procedure to repair the car. </p> 
    *@return Is returned an array with the car the we start to repair.
    */
    @Override
    public synchronized int[] startRepairProcedure(int threadID) {
      setMechanicState("FTC", threadID);
      if(orderFill && !suspended.isEmpty()){ //
        int[] temp = suspended.remove();
        logger();
        return temp;
      }else if(!fix_list.isEmpty()){
        return fix_list.remove();
      }else{
          int[] temp = new int[nparts + 1];
          temp[0] = -1;
          return temp;
      }
    }

   
    /**
    * <p> If there is enougth Parts to repair the car, the Mechanic take that parts from the stock. </p> 
    * @param car Car is an array, on what the first index is the id of the car, and the rest is the parts needed to repair this car.
    * @return Is returned an array with the number of parts that need to be repair the car.
    */
    @Override
    public synchronized int[] getRequiredPart(int[] car) {
      int [] part_toRepair = new int[nparts];
      for(int i = 0; i < stock.length ;i++){
          part_toRepair[i] = stock[i] - car[i+1];
          stock[i] = stock[i]-car[i+1];
      }
      setPartStock(stock);

      return part_toRepair;
    }
    
    /**
    * <p> If there is enougth Parts to repair the car, send information saying that, 
    * if not then send an order to Manager asking for more . </p> 
    * @param car Car is an array, on what the first index is the id of the car, and the rest is the parts needed to repair this car.
    * @return Is returned an array with the number of parts,in lack, that need to be repair the car.
    */
    @Override
    public synchronized int[] partAvailable(int[] car, int threadID) {
      setMechanicState("CST", threadID);
      int order_tmp[] = new int[nparts];
      boolean failed = false;
      for(int i = 0; i < stock.length ;i++){
        if(car[i+1] > stock[i]  ){
            failed = true;
            order_tmp[i] =  car[i+1];
        }  
      }
      if(failed){
        orderFill = false;
        return order_tmp;
      }
      order_tmp[0] = -1;
      return order_tmp;
    }

    
    
    /**
    * <p>Used to change the state of the mechanic. </p> 
    */
    @Override
    public synchronized void resumeRepairProcedure(int threadID) {
        setMechanicState("FTC", threadID);
        return;
    }
    
    
    
   
    /**
    * <p> Mechanic put the car in suspense when he has no parts to repair this car . </p> 
    *@param car Car is an array, on what the first index is the id of the car, and the rest is the parts needed to repair this car.
    */
    @Override
    public synchronized void putCarWaiting(int []car) {
        suspended.add(car);
        logger();
        
    }
    
  
    /**
    * <p> Mechanic fix the car  . </p> 
    *@param car Car is an array, on what the first index is the id of the car, and the rest is the parts needed to repair this car.
    *@param part_toRepair array with parts needed to rapir the car, after take of the store.
    *@return An array, on what the first index is the id of the car, and the rest is the parts fix of the car.
    */
    @Override
    public synchronized int[] fixIt(int [] car, int [] part_toRepair){
      for(int i = 0; i < stock.length ;i++){
        car[i+1] = car[i+1] - part_toRepair[i];
      }
      this.carCount++; // incrementa o numero de carros compostos para no fim matar o mechanico
    
      return car;
    }

    
    // --------------------------------- Manager Methods ---------------------------------
    

    /**
    * <p> Managers stores the new stock order, to mechanic then use. </p> 
    *@param new_parts array of parts ordered by mechanic to refill the stock.
    */
    @Override
    public synchronized void storePart(int[] new_parts){
        setManagerState("RPS");
        String [] parts_stock_replace = new String[nparts];
        Arrays.fill(parts_stock_replace,"F");
        if(new_parts.length != 3){
            throw new RuntimeException("Wrong part number to store in the repair area");
        }
        for(int i = 0; i < new_parts.length; i++){
            stock[i] += new_parts[i];
        }
        orderFill = true;
        setNotifications(parts_stock_replace);
        notifyAll();
    }
    
    /**
    * <p> Manager talked with one customer, and now he is registering 
    * the service to then the Mechanic work on that.  </p> 
    * @param car id of the car to be worked on.
    */
    @Override
    public synchronized void registerService(int car){
      setManagerState("PSJ");
      int [] parts = new int[nparts] ;
      int [] car_parts = new int[1+nparts];
      parts = this.fillParts();


      for(int i = 0; i < car_parts.length; i++){
        if(i == 0){
          car_parts[i] = car;
        }
        else{
          car_parts[i] = parts[i-1];
        }
      }
      fix_list.add(car_parts); 
      notifyAll();  // Acorda o Mechanic a dizer vai  trabalhar !! 
      increaseManagerRequests();
      
     
    }

    
    
// ------------------------------------------------------------
// ----------------- AUX FUNCS --------------------------------

    /**
    * <p> This function is to notify the two mechanics to say that the day is over, and can call out the day. </p> 
    */
    @Override
    public synchronized void wakeUpPal(){
      this.end_ofDay = true;
      notifyAll();
    }

    /** 
    * <p> This function is to effects of logging, we use this to set waht parts are we in need
    * and to set the number of parts that we asked for the suspend cars. </p> 
    */
    private void logger(){
      int []part_required = new int[nparts];
      String []part_requiredBool = new String [nparts]; 
      for(int i = 0; i < suspended.size() ;i++){
        int[] car = suspended.remove();
        for(int j = 0; j < stock.length; j++){
          part_required[j] += car[j+1];
        } 
        suspended.add(car);  
      }

      int[] temp = new int[nparts];
      for(int b = 0; b < stock.length; b++){
        if(part_required[b] > 0){
          temp[b]++;
        }
      }
      setPartRequirements(temp);

      for(int i = 0; i < part_required.length; i++){
        if(part_required[i] > stock[i] ){
            part_requiredBool[i] = "T";
        }
        else{
          part_requiredBool[i] = "F";
        }
      }
      setNotifications(part_requiredBool);
      
    }

    /** 
    * <p> Small functions to fill the number of parts and the type  that are with malfunction, and we 
    * basically have a 25% of probability of having 1,2,3 parts with malfuction of the same type. </p> 
    *@return  an array with the number of parts that have mail funcions
    */
    private synchronized int[] fillParts(){
      int [] parts = new int[nparts] ;
      Random gerador = new Random();
      Double prob = 0.0;
      for(int i = 0; i < nparts; i++){
          prob = gerador.nextDouble();
          if(prob <= 0.25){
              parts[i] = 0;
          }  
          if(prob >0.25 && prob <= 0.5){
            parts[i] = 1;
          }
          if(prob >0.5 && prob <= 0.75){
            parts[i] = 2;
          }
          if(prob > 0.75 && prob < 1.0){
            parts[i] = 3;
          }
          
      }

      return parts;
    }


    /** 
    * <p> Used to count the number of car repaired. </p> 
    *@return  number of cars that were rapaired,
    */
    @Override
    public synchronized int getCarCount(){
      return this.carCount;
    }

    /** 
    * <p> Used to verify if already repair all the cars and if any car is in the suspend list,
    * and if all order of works are done. </p> 
    *@return  number of cars that were rapaired,
    */
    @Override
    public synchronized boolean workdone(){
      if((this.carCount > 29) && suspended.isEmpty() && fix_list.isEmpty()){
        return false;
      }
      return true;
    }
    
    public synchronized void setDone(){
        this.done = true;
    }
    
    public synchronized boolean getDone(){
        return this.done;
    }

    
    // Repository methods
    
    private synchronized void setManagerState(String t){
        GeneralRepositoryMessages response;
        openComm(gr,"Repair Area General Repository Update method.");
        gr.writeObject(new GeneralRepositoryMessages(GeneralRepositoryMessages.SET_MANAGER_STATE));
        response = (GeneralRepositoryMessages) gr.readObject(); 
        gr.close();
    }
    
    private synchronized void setMechanicState(String t,int threadID){
        GeneralRepositoryMessages response;
        openComm(gr,"Lounge General Repository Update method.");
        gr.writeObject(new GeneralRepositoryMessages(GeneralRepositoryMessages.SET_MECHANIC_STATE, t,threadID));
        response = (GeneralRepositoryMessages) gr.readObject(); 
        gr.close();
    }
    
    private synchronized void setNotifications( String [] parts_stock_replace){
        GeneralRepositoryMessages response;
        openComm(gr,"Lounge General Repository Update method.");
        gr.writeObject(new GeneralRepositoryMessages(GeneralRepositoryMessages.SET_NOTIFICATIONS,parts_stock_replace));
        response = (GeneralRepositoryMessages) gr.readObject(); 
        gr.close();
    }
    
    private synchronized void setPartStock( int [] stock){
        GeneralRepositoryMessages response;
        openComm(gr,"Lounge General Repository Update method.");
        gr.writeObject(new GeneralRepositoryMessages(GeneralRepositoryMessages.SET_PART_STOCK,stock));
        response = (GeneralRepositoryMessages) gr.readObject(); 
        gr.close();
    }
    
    private synchronized void setPartRequirements( int [] temp){
        GeneralRepositoryMessages response;
        openComm(gr,"Lounge General Repository Update method.");
        gr.writeObject(new GeneralRepositoryMessages(GeneralRepositoryMessages.SET_PART_REQUIREMENTS,temp));
        response = (GeneralRepositoryMessages) gr.readObject(); 
        gr.close();
    }
    
    private synchronized void increaseManagerRequests(){
        GeneralRepositoryMessages response;
        openComm(gr,"Lounge General Repository Update method.");
        gr.writeObject(new GeneralRepositoryMessages(GeneralRepositoryMessages.INCREASE_MANAGER_REQUESTS));
        response = (GeneralRepositoryMessages) gr.readObject(); 
        gr.close();
    }

    private void openComm(ClientComm cc, String name){
        while(!cc.open()){
            System.out.println(name + " not open, trying again...");
            try{
                Thread.sleep(1000);
            }catch(Exception ex){ }            
        }
    }
    

}
