/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package RepairAreaMain.Interfaces;

/**
 *
 * @author diniscanastro
 */
public interface ManagerLoungeInterface {

    /**
     * <p> Manager decides from the array of tasks he has available which one to complete:</p>
     * <p> 1 - Customers to attend at the reception. </p>
     * <p> 2 - Customers to notify the car is ready. </p>
     * <p> 3 - Part's order to retrieve. </p>
     * <p> 4 - Customers are waiting for a replacement car. </p>
     * <p> In this method we are able to control the priority of the tasks the manager will complete.</p>
     * @return The task that will be completed in the form of the identifying integer.
     */
    int appraiseSit();

    // ----------------------------------------------- Manager Methods -----------------------------------------------
    /**
     * <p> Manager waits to be awaken with tasks to be fulfilled.
     * </p>
     */
    void getNextTask();

    /**
     * <p>
     *   Retrieves the parts required to order from the supply site.
     * </p>
     * @return Returns the current required order list.
     */
    int[] getPartsOrders();

    /**
     * <p>
     * Manager wakes up and delivers a replacement car key to a customer waiting on the queue for a replacement car.
     * </p>
     *
     */
    void handCarKey();

    /**
     * <p>
     *   Retrieves the next client to notify from the notifications pending queue.
     * </p>
     * @return Returns the ID of the Customer to notify.
     */
    int notifyClient();

    /*
    Receive the payment and possibly a replacement vehicle back
     */
    /**
     * <p>
     * Manager gathers the information when a customer is paying. </p>
     * <p> The information retrieved includes information whether the customer used a replacement car and if it is returning it.
     * </p>
     *
     */
    void receivePayment();

    /**
     * <p>
     *   Mechanic notifies the manager that a repair is finished by adding to a queue the car identifying number for the manager
     * to notify the customer when available.
     * </p>
     * @param car identifies the car that finished repairing.
     */
    void repairConcluded(int car, int threadID);

    /*
    [Manager] Talks with customers with 2 options:
    - Deliver their car for repair (with the possibility of requiring for a replacement car) TODO
    - Receive the payment for a repair already concluded TODO
    Parameters required:
    - If it's payment, repair or hand out key
     */
    /**
     * <p>
     * Manager receives customers. A customer is awaken to negotiate his parameters with the manager.</p>
     * <p>
     * It gathers whether the customer is paying or requesting a service.
     * </p>
     * @return Returns the car ID to be repaired or '-1' if it was a payment talk.
     */
    int talkToCustomer();
    
}
