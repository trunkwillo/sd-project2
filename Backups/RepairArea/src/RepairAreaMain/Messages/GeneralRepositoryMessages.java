/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RepairAreaMain.Messages;

import java.io.Serializable;
/**
 *
 * @author bmend
 */
public class GeneralRepositoryMessages implements Serializable {
    private int msgType = -1;
    private boolean msgState = false;
    

    public static final int SET_MANAGER_STATE = 1; 
    public static final int SET_CUSTOMER_STATE = 3; 
    public static final int SET_MECHANIC_STATE = 4; 
    public static final int SET_CUSTOMER_CAR = 5; 
    public static final int SET_REPLACEMENT_NEEDED = 6; 
    public static final int SET_REPAIR_DONE = 7; 
    public static final int SET_ENTRANCE_QUEUE = 8; 
    public static final int SET_REPLACEMENT_QUEUE = 9; 
    public static final int INCREASE_REPAIR_COUNT = 10; 
    public static final int INCREASE_CUSTOMER_PARKED = 11; 
    public static final int DECREASE_CUSTOMER_PARKED = 12; 
    public static final int INCREASE_REPLACEMENT_PARKED = 13;
    public static final int DECREASE_REPLACEMENT_PARKED = 14;
    public static final int INCREASE_MANAGER_REQUESTS = 15;
    public static final int SET_PART_STOCK = 16;
    public static final int SET_PART_REQUIREMENTS = 17;
    public static final int SET_NOTIFICATIONS = 18;
    public static final int SET_ORDERS_FILLED = 19;
    /**
     * Message to signal the client that his request has been processed with success
     */
    public static final int OK = 20;
    
    /**
     * Message to terminate server activity
     */
    public static final int END = 0;

    
    private String state;

    // Customer info
    private String[] car_driven = new String[30 + 3];           // ID of the driven car | Customer's - "Customer ID" / Replacement - "R0x" / None - "-"
    private String[] replacement_needed = new String[30];   // Customer requires/required replacement car | (T)rue or (F)alse
    private String[] repair_done = new String[30];          // Customer car is already fixed | (T)rue or (F)alse
    
    // Monitor states
    // Lounge
    private int entrance_qq = 0;
    private int replacement_qq = 0;
    private int cars_repaired_count = 0;
    
    // Park
    private int customer_cars_parked = 0;
    private int replacement_cars_parked = 3;
    
    // Repair Area
    private int requests_by_manager = 0;                            // Counter of the number of requests made by the manager
    private int[] part_stock = new int[3];                          // Number of part x in stock
    private int[] part_required = new int[3];                       // Number of part x required to resume the fixes
    private String[] manager_notified = new String[3];              // Part X missing notification sent | (T)rue or (F)alse
    
    private int threadID;
    // Supplier site
    private int[] parts_ordered = new int[3];               // Counting part x number of orders completed
    
    public GeneralRepositoryMessages(int msgType) {
        this.msgType = msgType;
    }
    
    public GeneralRepositoryMessages(int msgType,String state) {
        this.msgType = msgType;
        this.state = state;
    }
    
    public GeneralRepositoryMessages(int msgType,String state, int threadID) {
        this.msgType = msgType;
        this.threadID = threadID;
        this.state = state;
        
    }
    
    public GeneralRepositoryMessages(int msgType,int size) {
        this.msgType = msgType;
        if(this.msgType == SET_ENTRANCE_QUEUE ){
             entrance_qq = size;
        }else if(this.msgType ==SET_REPLACEMENT_QUEUE){
             replacement_qq = size;
        }
        
    }
    
    public GeneralRepositoryMessages(int msgType, int[] parts) {
        this.msgType = msgType;
        if(this.msgType == SET_PART_STOCK ){
            part_stock = parts;
        }else if(this.msgType ==SET_PART_REQUIREMENTS){
             part_required = parts;
        }else if(this.msgType ==SET_ORDERS_FILLED){  
            parts_ordered= parts; 
        }
        
        
      
    }
    
    public GeneralRepositoryMessages(int msgType, String[] flags){
       this.msgType = msgType;
        if(this.msgType == SET_NOTIFICATIONS ){
            manager_notified = flags;
        } 
    
    }
    
    public int getThreadID(){
        return this.threadID;
        
    }
    
    public int getMsgType() {
        return msgType;
    }

    public boolean isMsgState() {
        return msgState;
    }

    public String getstate() {
        return this.state;
    }

    public String[] getCar_driven() {
        return car_driven;
    }

    public String[] getReplacement_needed() {
        return replacement_needed;
    }

    public String[] getRepair_done() {
        return repair_done;
    }

    public int getEntrance_qq() {
        return entrance_qq;
    }

    public int getReplacement_qq() {
        return replacement_qq;
    }

    public int getCars_repaired_count() {
        return cars_repaired_count;
    }

    public int getCustomer_cars_parked() {
        return customer_cars_parked;
    }

    public int getReplacement_cars_parked() {
        return replacement_cars_parked;
    }

    public int getRequests_by_manager() {
        return requests_by_manager;
    }

    public int[] getPart_stock() {
        return part_stock;
    }

    public int[] getPart_required() {
        return part_required;
    }

    public String[] getManager_notified() {
        return manager_notified;
    }

    // States
    public int[] getParts_ordered() {
        return parts_ordered;
    }
    
    
    
   
    
}
