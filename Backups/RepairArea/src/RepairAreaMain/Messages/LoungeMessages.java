/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package RepairAreaMain.Messages;

import java.io.Serializable;
/**
 *
 * @author diniscanastro
 */
public class LoungeMessages implements Serializable{
    
    private int msgType = -1;
    private boolean msgState = false;
    
    public static final int GET_REQUIRED_PART = 1;
    public static final int APPRAISE_SITUATION = 2;
    public static final int TALK_TO_CUSTOMER = 3;
    public static final int RECEIVE_PAYMENT = 4;
    public static final int HAND_CAR_KEY = 5;
    public static final int QUEUE_IN = 6;
    public static final int TALK_WITH_MANAGER = 7;
    public static final int PAY_FOR_SERVICE = 8;
    public static final int COLLECT_KEY = 9;
    public static final int LET_MANAGER_KNOW = 10;
    public static final int REPAIR_CONCLUDED = 11;
    public static final int GET_NEXT_TASK = 12;
    public static final int NOTIFY_CLIENT = 13;
    
    /**
     * Message to signal the client that his request has been processed with success
     */
    public static final int OK = 20;
    
    /**
     * Message to terminate server activity
     */
    public static final int END = 0;
    
    private int threadID;
    private int replacement;
    private int car;
    private int[] stock;
    
    // Responses
    private int response;
    
    /**
     * Constructor with the message type
     * @param msgType type of message received
     */
    public LoungeMessages(int msgType) {
        this.msgType = msgType;
    }
    
    public LoungeMessages(int msgType, int threadID) {
        this.msgType = msgType;
        this.threadID = threadID;
    }
    
    public LoungeMessages(int msgType, int threadID, int var) {
        this.msgType = msgType;
        this.threadID = threadID;
        if(msgType == REPAIR_CONCLUDED){
            this.car = var;
        }else if(msgType == PAY_FOR_SERVICE){
            this.replacement = var;
        }
        
    }
    
    public LoungeMessages(int msgType, int threadID, int[] stock) {
        this.msgType = msgType;
        this.threadID = threadID;
        this.stock = stock;
    }
    
    
    /***
     * 
     * @param msgType
     * @param msgState 
     * Response
     */
    public LoungeMessages(int msgType, boolean msgState) {
        this.msgType = msgType;
        this.msgState = msgState;
    }
    
    /***
     * 
     * @param msgType
     * @param msgState 
     * @param response
     * Response
     */
    public LoungeMessages(int msgType, boolean msgState, int response) {
        this.msgType = msgType;
        this.msgState = msgState;
        this.response = response;
    }
    
    
     /**
     * Getter method returning the message type
     * @return integer value representing the message type
     */
    public int getMessageType(){
        return this.msgType;  
    }
    
    /**
     * Getter method returning the state of the message
     * @return boolean value signaling the state of the message
     */
    public boolean getMessageState(){
        return this.msgState;
    }
    
    public int getMessageReplacement(){
        return this.replacement;
    }
    
    public int getMessageThreadID(){
        return this.threadID;
    }
    
    public int getMessageCar(){
        return this.car;
    }
    
    public int[] getMessageStock(){
        return this.stock;
    }
    
    
    
}
