/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RepairAreaMain;

import RepairAreaMain.CommunicationChannel.ServerComm;
import RepairAreaMain.Messages.RepairAreaMessages;
/**
 *
 * @author bmend
 */
public class RepairAreaProxy implements Runnable{

     // Communication
    ServerComm socket;
    RepairArea ra;
    
    public RepairAreaProxy(ServerComm socket, RepairArea ra) {
        this.socket = socket;
        this.ra = ra;
    }
    
    
    @Override
    public void run(){
        
        RepairAreaMessages msg_received, msg_sent = null;
        
        msg_received = (RepairAreaMessages) socket.readObject();
        
        int response_int;
        int[] response_array;
        
        // Processing the message method
        switch (msg_received.getMessageType()) {
            case RepairAreaMessages.READ_THE_PAPER:
                ra.readThePaper(msg_received.getMessageThreadID());
                msg_sent = new RepairAreaMessages(RepairAreaMessages.OK);
                break;
                
            case RepairAreaMessages.START_REPAIR_PROCEDURE:
                response_array = ra.startRepairProcedure(msg_received.getMessageThreadID());
                msg_sent = new RepairAreaMessages(RepairAreaMessages.OK,response_array);
                break;
                 
            case RepairAreaMessages.GET_REQUIRED_PART:
                response_array = ra.getRequiredPart(msg_received.getMessageCar());
                msg_sent = new RepairAreaMessages(RepairAreaMessages.OK,response_array);
                break;
            
            case RepairAreaMessages.PART_AVAILABLE:
                response_array = ra.partAvailable(msg_received.getMessageCar(),msg_received.getMessageThreadID());
                msg_sent = new RepairAreaMessages(RepairAreaMessages.OK,response_array);
                break;
             
            case RepairAreaMessages.RESUME_REPAIR_PROCEDURE:
                ra.resumeRepairProcedure(msg_received.getMessageThreadID());
                msg_sent = new RepairAreaMessages(RepairAreaMessages.OK);
                break;
                
            case RepairAreaMessages.PUT_CAR_WAITING:
                ra.putCarWaiting(msg_received.getMessageCar());
                msg_sent = new RepairAreaMessages(RepairAreaMessages.OK);
                break;
                
            case RepairAreaMessages.FIX_IT:
                response_array = ra.fixIt(msg_received.getMessageCar(),msg_received.getMessagePartToRepair());
                msg_sent = new RepairAreaMessages(RepairAreaMessages.OK,response_array);
                break;
                
            case RepairAreaMessages.STORE_PART:
                ra.storePart(msg_received.getMessageNewParts());
                msg_sent = new RepairAreaMessages(RepairAreaMessages.OK);
                break;
                
            case RepairAreaMessages.REGISTER_SERVICE:
                ra.registerService(msg_received.getMessageCarID());
                msg_sent = new RepairAreaMessages(RepairAreaMessages.OK);
                break;
            
            case RepairAreaMessages.WAKE_UP_PAL:
                ra.wakeUpPal();
                msg_sent = new RepairAreaMessages(RepairAreaMessages.OK);
                break;
                
            case RepairAreaMessages.END:
                System.out.println("END");
                ra.setDone();
                msg_sent = new RepairAreaMessages(RepairAreaMessages.OK);
                throw new RuntimeException("Received end message.");
                
            default:
                System.out.println("[ERROR] Received invalid message type: " + msg_received);
                break;
        }
    }
    
}
