/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ManagerMain;

import ManagerMain.CommunicationChannel.ClientComm;
import static ManagerMain.CommunicationChannel.CommPorts.*;
        
        
/**
 *
 * @author diniscanastro
 */
public class Manager implements Runnable{
    
    private int threadID;
    int post_it_description;
    int post_it_info;
    ManagerMethods mm = new ManagerMethods();
    int customer_count = 0;
    

    public Manager(int threadID){
      this.threadID = threadID;
    }
    
    @Override
    public void run(){
        while(customer_count < 30){
            mm.getNextTask(threadID);
            switch(mm.appraiseSit(threadID)){
                case 1: // Talk with customers
                    //gr.setManagerState("ATC");
                    int decision = mm.talkToCustomer(threadID);
                    if(decision >= 0 && decision <= 29){          // ID of car to repair
                        mm.registerService(decision,threadID);
                        //gr.setManagerState("PSJ");
                    }else if(decision == -1){   // Receive Payment
                        mm.receivePayment(threadID);
                        customer_count++;
                    }
                    break;
                case 2: // Stock to refill 
                    int[] order = mm.getPartsOrders(threadID);
                    // Get more parts than the requireds
                    order = addExtra(order);
                    if(!mm.goToSupplierSite(order,threadID)){
                        throw new RuntimeException("Suplier site failed horribly!");
                    }
                    //gr.setManagerState("GNP");
                    mm.storePart(order,threadID);
                    //gr.setManagerState("RPS");
                    break;
                case 3: // Customers to notify
                    int tmp = mm.notifyClient(threadID);
                    if(tmp == -1){
                        break;
                    }
                    mm.phoneCustomer(tmp,threadID);
                    //gr.setManagerState("ALC");
                    break;
                case 4: // Hand car keys 
                    //gr.setManagerState("ATC");
                    mm.handCarKey(threadID);
                    break;
                default:
                    throw new RuntimeException("Task with wrong or undefined description");
            }
            //gr.setManagerState("CWD");
        }
        
        // Closing all monitors
        mm.closeAll(threadID);
    }
    
    // ---------------------- Auxiliary Methods -------------------------
    
    /**
    * <p> 
    *   Increases the amount of parts on the order.
    *   This decreases the amount of trips made to the supplier site.
    * </p>
    * @param order has information relative to the quantity required of each part.
    * @return Returns the current required order list increased by the extra amount.
    */
        private int[] addExtra(int[] order){
            for(int i = 0; i < order.length; i++){
                order[i] += 5;
            }
            return order;
        }
    
    
}
