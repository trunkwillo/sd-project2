/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ManagerMain.Interfaces;

/**
 *
 * @author diniscanastro
 */
public interface ManagerRepairAreaInterface {

    /**
     * <p> Manager talked with one customer, and now he is registering
     * the service to then the Mechanic work on that.  </p>
     * @param car id of the car to be worked on.
     */
    void registerService(int car, int threadID);

    // --------------------------------- Manager Methods ---------------------------------
    /**
     * <p> Managers stores the new stock order, to mechanic then use. </p>
     *@param new_parts array of parts ordered by mechanic to refill the stock.
     */
    void storePart(int[] new_parts, int threadID);
    
}
