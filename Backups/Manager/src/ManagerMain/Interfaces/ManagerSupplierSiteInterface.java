/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ManagerMain.Interfaces;

/**
 *
 * @author diniscanastro
 */
public interface ManagerSupplierSiteInterface {

    boolean goToSupplierSite(int[] order, int threadID);
    
}
