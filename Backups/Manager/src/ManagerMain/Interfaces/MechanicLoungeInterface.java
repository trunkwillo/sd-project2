/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ManagerMain.Interfaces;

/**
 *
 * @author diniscanastro
 */
public interface MechanicLoungeInterface {

    // ----------------------------------------------- Mechanics Methods -----------------------------------------------
    /*
    [Mechanic] Warning the manager there are parts missing in stock
     */
    /**
     * <p>
     *   Mechanic sums up the parts that are missing for the manager to pick up when possible.
     * </p>
     * @param stock_needed declares the amount of parts of each type that it's required.
     */
    void letManagerKnow(int[] stock_needed, int threadID);

    /**
     * <p>
     *   Mechanic notifies the manager that a repair is finished by adding to a queue the car identifying number for the manager
     * to notify the customer when available.
     * </p>
     * @param car identifies the car that finished repairing.
     */
    void repairConcluded(int car, int threadID);
    
}
