/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LoungeMain;

//import WorkShop.AuxClasses.Car;
import LoungeMain.CommunicationChannel.ClientComm;
import LoungeMain.Interfaces.CustomerLoungeInterface;
import LoungeMain.Interfaces.ManagerLoungeInterface;
import LoungeMain.Interfaces.MechanicLoungeInterface;
import LoungeMain.Messages.GeneralRepositoryMessages;
import static ManagerMain.CommunicationChannel.CommPorts.*;


import genclass.GenericIO;
import java.util.*;



public class Lounge implements CustomerLoungeInterface, MechanicLoungeInterface, ManagerLoungeInterface{
    
    // Counters
    private int replacement_cars_amount;
    private boolean[] replacement_cars;
    
    // Arrays
    private int[] parts_orders;
    
    // Queues
    private Queue<Integer> entrance_queue = new LinkedList<>();     // Customers enter the lounge area
    private Queue<Integer> notification_queue = new LinkedList<>(); // Keeps ID of the ready cars
    private Queue<Integer> replacement_queue = new LinkedList<>();  // Keeps ID of the ready cars
    private Queue<Integer> fix_orders = new LinkedList<>();         // Orders pending to send to the repair area
    
    // Wake up flags
    private boolean manager_receiving_customers = false;
    private boolean manager_handing_keys = false;
    private boolean negotiation_ready = false;
    
    // Dialog Manager <-> Customer
    //Payment
    private int replacementID = -2;
    private boolean paying = false;
    
    private int repair_order = -1;
    
    
    // Repository
    ClientComm gr;
    
    // Status
    private boolean done = false;

    
    // Constructor
    public Lounge(int replacement_qty, int parts){
        this.gr = new ClientComm(GENERAL_REPOSITORY_NAME, GENERAL_REPOSITORY_PORT);
        this.replacement_cars_amount = replacement_qty;
        replacement_cars = new boolean[replacement_qty];
        parts_orders = new int[parts];
        Arrays.fill(replacement_cars, true);
    }
 
    
    
    
    // ----------------------------------------------- Manager Methods -----------------------------------------------
    
    /**
    * <p> Manager waits to be awaken with tasks to be fulfilled.
    * </p>
    */
    @Override
    public synchronized void getNextTask(){
        setManagerState("CWD");

        // If there's nothing todo, wait until someone wakes him up
        while(entrance_queue.isEmpty() && !stockRequired() && notification_queue.isEmpty() && (replacement_queue.isEmpty() || replacement_cars_amount == 0)){
            try{
                wait();
            }catch(InterruptedException ex){
                Thread.currentThread().interrupt();
            }
        }
    }
    
    /**
    * <p> Manager decides from the array of tasks he has available which one to complete:</p>
    * <p> 1 - Customers to attend at the reception. </p>
    * <p> 2 - Customers to notify the car is ready. </p>
    * <p> 3 - Part's order to retrieve. </p>
    * <p> 4 - Customers are waiting for a replacement car. </p>
    * <p> In this method we are able to control the priority of the tasks the manager will complete.</p>
    * @return The task that will be completed in the form of the identifying integer.
    */
    @Override
    public synchronized int appraiseSit(){
        if(!entrance_queue.isEmpty()){                                                  // Customers in the main queue
            return 1;
        }else if(stockRequired()){                                                      // Stock required
            return 2;
        }else if(!notification_queue.isEmpty()){                                        // Customer ready to be notified
            return 3;
        }else if(!replacement_queue.isEmpty() && replacement_cars_amount > 0){          // Customer waiting for replacement car ready
            return 4;
        }
        return 0;
    }
    
    /*
        [Manager] Talks with customers with 2 options:
            - Deliver their car for repair (with the possibility of requiring for a replacement car) TODO
            - Receive the payment for a repair already concluded TODO
    
            Parameters required:
                - If it's payment, repair or hand out key
    */
    /**
    * <p> 
    * Manager receives customers. A customer is awaken to negotiate his parameters with the manager.</p>
    * <p>
    * It gathers whether the customer is paying or requesting a service.
    * </p>
    * @return Returns the car ID to be repaired or '-1' if it was a payment talk.
    */
    @Override
    public synchronized int talkToCustomer(){
        // Wakes up first customer in the queue
        setManagerState("ATC");
        manager_receiving_customers = true;
        notifyAll();
        
        // Waits for customer to define negotiation parameters
        while(!negotiation_ready){
            try{
                wait();
            }catch(InterruptedException ex){
                Thread.currentThread().interrupt();
            }
        }
        // Flag reset
        negotiation_ready = false;
        
        if(paying){
            return -1;
        }else{
            return repair_order;
        }
    }
    
    /*
        Receive the payment and possibly a replacement vehicle back
    */
    
    /**
    * <p>
    * Manager gathers the information when a customer is paying. </p>
    * <p> The information retrieved includes information whether the customer used a replacement car and if it is returning it.
    * </p>
    * 
    */
    @Override
    public synchronized void receivePayment(){
        if(replacementID >= 30 && replacementID <= 32){
            replacement_cars[replacementID - 30] = true;
            replacement_cars_amount++;
        }
        replacementID = -2;
    }
    
    /**
    * <p> 
    * Manager wakes up and delivers a replacement car key to a customer waiting on the queue for a replacement car.
    * </p>
    *
    */
    @Override
    public synchronized void handCarKey(){
        setManagerState("ATC");
        int index = replacementAvailable();
        if(index >= 0 && index <= 2){
            replacement_cars[index] = false;
            replacement_cars_amount--;
            replacementID = index + 30;
            manager_handing_keys = true;
            notifyAll();
        }
    }
        
    
    // ----------------------------------------------- Customer Methods ------------------------------------------------
    
    
    /**
    * <p> 
    * Customer queues in to talk with manager.
    * Can be awaken by the manager to proceed with parameters negotiation.
    * </p>
    * @see talkToCustomer()
    */
    @Override
    public synchronized void queueIn(int threadID){
        notifyAll();                        // Acordar manager caso esteja sem nada para fazer
        setCustomerState("RCP", threadID);
        
        setEntranceQueue(entrance_queue.size()); // Logging
        
        entrance_queue.add(threadID);
        
        while(entrance_queue.peek() != threadID || !manager_receiving_customers){
            try{
                wait();
            }catch(InterruptedException ex){
                Thread.currentThread().interrupt();
            }
        }
        manager_receiving_customers = false;
        entrance_queue.remove();
        
        setEntranceQueue(entrance_queue.size()); // Logging
    }
    
    /*
        Customer comes to post a job offer
    */
    
    /**
    * <p> 
    * Customer sets the shared parameters to talk with the Manager for a car repair.
    * Shares the Car ID with the manager.
    * </p>
    * @param threadID used to identify the car with the customer ID.
    * @see talkToCustomer()
    */
    @Override
    public synchronized void talkWithManager(int threadID){
        // Fills form
        paying = false;
        repair_order = threadID;
       
        
        // Wakes up manager
        negotiation_ready = true;
        notifyAll();
    }
    
    /*
        Customer comes to pay and warning if he has replacement car or not
    */
    
    /**
    * <p> 
    * Customer sets the shared parameters to talk with the Manager for a service payment.
    * Can deliver the replacement car if it's suitable.
    * </p>
    * @param replacement used to identify the replacement car the customer is returning.
    * @see receivePayment()
    */
    @Override
    public synchronized void payForTheService(int replacement){
        // Fills form
        paying = true;
        this.replacementID = replacement;
        
        negotiation_ready = true;
        
        // Wakes up manager
        notifyAll(); 
    }
    
    
    /**
    * <p> 
    * Customer awaits for the manager to deliver him a replacement car key on a queue.
    * </p>
    * @param threadID used to identify the customer on the queue and verify if he is the first.
    * @see handCarKey()
    */
    @Override
    public synchronized int collectKey(int threadID){
        notifyAll();                        // Acordar manager caso esteja sem nada para fazer
        
        setReplacementQueue(replacement_queue.size());
        setCustomerState("WRC", threadID);
        
        replacement_queue.add(threadID);
        
        while(replacement_queue.peek() != threadID || !manager_handing_keys){
            try{
                wait();
            }catch(InterruptedException ex){
                Thread.currentThread().interrupt();
            }
        }
        
        //System.out.println("---------- VALUE OF REPLACEMENT ID: " + replacementID);
        manager_handing_keys = false;
        replacement_queue.remove();
        
        setReplacementQueue(replacement_queue.size());
        
        setCustomerCar(String.valueOf(replacementID), threadID);
        return replacementID;
        
    }
    
    // ----------------------------------------------- Mechanics Methods -----------------------------------------------
    
    /*
      [Mechanic] Warning the manager there are parts missing in stock
    */
    
    /**
    * <p> 
    *   Mechanic sums up the parts that are missing for the manager to pick up when possible.
    * </p>
    * @param stock_needed declares the amount of parts of each type that it's required.
    */
    @Override
    public synchronized void letManagerKnow(int[] stock_needed, int threadID){
        setMechanicState("ALM", threadID);
        if(stock_needed.length != 3){
          throw new RuntimeException("Stock order is not the correct size");
        }

        for(int i = 0; i < parts_orders.length; i++){
            this.parts_orders[i] += stock_needed[i];
        }

        notifyAll(); // Caso manager esteja à espera de alguma coisa para fazer
    }

    
    /**
    * <p> 
    *   Mechanic notifies the manager that a repair is finished by adding to a queue the car identifying number for the manager
    * to notify the customer when available.
    * </p>
    * @param car identifies the car that finished repairing.
    */
    @Override
    public synchronized void repairConcluded(int car, int threadID){
      setRepairDone("T", car);
      setMechanicState("ALM", threadID);
      notification_queue.add(car);
      increaseRepairedCount();
      notifyAll(); // Caso manager esteja à espera de alguma coisa para fazer
    }
    
    // ----------------------------------------------- Auxiliary Methods -----------------------------------------------
    
    /**
    * <p> 
    *   Navigates the part's orders searching if there are any parts requiring pick up from the supplier site.
    * </p>
    * @return Returns true if there are any parts requiring ordering from the supply site.
    */
    private synchronized boolean stockRequired(){
        for(int i = 0; i < parts_orders.length; i++){
            if(parts_orders[i] > 0) return true;
        }
        return false;
    }
    
    /**
    * <p> 
    *   Retrieves the next client to notify from the notifications pending queue.
    * </p>
    * @return Returns the ID of the Customer to notify. 
    */
    @Override
    public synchronized int notifyClient(){
        if(notification_queue.size() > 0){
            return notification_queue.remove();
        }
        return -1;
    }
    
    /**
    * <p> 
    *   Retrieves the parts required to order from the supply site.
    * </p>
    * @return Returns the current required order list. 
    */
    @Override
    public synchronized int[] getPartsOrders(){
        int[] temp = Arrays.copyOf(parts_orders, parts_orders.length);
        Arrays.fill(parts_orders, 0);
        return temp;
    }
    
    /**
    * <p> 
    *   Verifies if there are replacement cars available to deliver to customers.
    * </p>
    * @return Returns the index of the available replacement cars or '-1' if there are no replacement cars available. 
    */
    private synchronized int replacementAvailable(){
        for(int i = 0; i < replacement_cars.length; i++){
            if(replacement_cars[i] == true){
                return i;
            }
        }
        return -1;
    }
    
    public synchronized void setDone(){
        this.done = true;
    }
    
    public synchronized boolean getDone(){
        return this.done;
    }
    
    
    // Repository methods
    
    private synchronized void setManagerState(String t){
        GeneralRepositoryMessages response;
        openComm(gr,"Lounge General Repository Update method.");
        gr.writeObject(new GeneralRepositoryMessages(GeneralRepositoryMessages.SET_MANAGER_STATE));
        response = (GeneralRepositoryMessages) gr.readObject(); 
        gr.close();
    }
    
    private synchronized void setCustomerState(String t, int threadID){
        GeneralRepositoryMessages response;
        openComm(gr,"Lounge General Repository Update method.");
        gr.writeObject(new GeneralRepositoryMessages(GeneralRepositoryMessages.SET_CUSTOMER_STATE, t,threadID));
        response = (GeneralRepositoryMessages) gr.readObject(); 
        gr.close();
    }
    
    private synchronized void setCustomerCar(String t, int threadID){
        GeneralRepositoryMessages response;
        openComm(gr,"Lounge General Repository Update method.");
        gr.writeObject(new GeneralRepositoryMessages(GeneralRepositoryMessages.SET_CUSTOMER_CAR, t,threadID));
        response = (GeneralRepositoryMessages) gr.readObject(); 
        gr.close();
    }
    
    private synchronized void setMechanicState(String t, int threadID){
        GeneralRepositoryMessages response;
        openComm(gr,"Lounge General Repository Update method.");
        gr.writeObject(new GeneralRepositoryMessages(GeneralRepositoryMessages.SET_MECHANIC_STATE, t,threadID));
        response = (GeneralRepositoryMessages) gr.readObject(); 
        gr.close();
    }
    
    private synchronized void setEntranceQueue(int size){
        GeneralRepositoryMessages response;
        openComm(gr,"Lounge General Repository Update method.");
        gr.writeObject(new GeneralRepositoryMessages(GeneralRepositoryMessages.SET_ENTRANCE_QUEUE, size));
        response = (GeneralRepositoryMessages) gr.readObject(); 
        gr.close();
    }
    
    private synchronized void setReplacementQueue(int size){
        GeneralRepositoryMessages response;
        openComm(gr,"Lounge General Repository Update method.");
        gr.writeObject(new GeneralRepositoryMessages(GeneralRepositoryMessages.SET_REPLACEMENT_QUEUE, size));
        response = (GeneralRepositoryMessages) gr.readObject(); 
        gr.close();
    }
    
    private synchronized void setRepairDone(String t , int carID){
        GeneralRepositoryMessages response;
        openComm(gr,"Lounge General Repository Update method.");
        gr.writeObject(new GeneralRepositoryMessages(GeneralRepositoryMessages.SET_REPAIR_DONE, t,carID));
        response = (GeneralRepositoryMessages) gr.readObject(); 
        gr.close();
    }
    
    private synchronized void increaseRepairedCount(){
        GeneralRepositoryMessages response;
        openComm(gr,"Lounge General Repository Update method.");
        gr.writeObject(new GeneralRepositoryMessages(GeneralRepositoryMessages.INCREASE_REPAIR_COUNT));
        response = (GeneralRepositoryMessages) gr.readObject(); 
        gr.close();
    }
    
    private void openComm(ClientComm cc, String name){
        while(!cc.open()){
            System.out.println(name + " not open, trying again...");
            try{
                Thread.sleep(1000);
            }catch(Exception ex){ }            
        }
    }
    
}