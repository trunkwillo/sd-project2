/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package LoungeMain.Interfaces;

/**
 *
 * @author diniscanastro
 */
public interface CustomerLoungeInterface {

    /**
     * <p>
     * Customer awaits for the manager to deliver him a replacement car key on a queue.
     * </p>
     * @param threadID used to identify the customer on the queue and verify if he is the first.
     * 
     */
    int collectKey(int threadID);

    /*
    Customer comes to pay and warning if he has replacement car or not
     */
    /**
     * <p>
     * Customer sets the shared parameters to talk with the Manager for a service payment.
     * Can deliver the replacement car if it's suitable.
     * </p>
     * @param replacement used to identify the replacement car the customer is returning.
     * 
     */
    void payForTheService(int replacement);

    /**
     * <p>
     * Customer queues in to talk with manager.
     * Can be awaken by the manager to proceed with parameters negotiation.
     * </p>
     * 
     */
    void queueIn(int threadID);

    /**
     * <p>
     * Customer sets the shared parameters to talk with the Manager for a car repair.
     * Shares the Car ID with the manager.
     * </p>
     * @param threadID used to identify the car with the customer ID.
     * 
     */
    void talkWithManager(int threadID);
    
}
