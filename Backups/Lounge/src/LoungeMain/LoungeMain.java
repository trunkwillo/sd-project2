/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LoungeMain;

import LoungeMain.CommunicationChannel.ServerComm;
import LoungeMain.LoungeProxy;
import static ManagerMain.CommunicationChannel.CommPorts.*;

/**
 *
 * @author bmend
 */
public class LoungeMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Lounge lg = new Lounge(3, 3);
        ServerComm socketCommunication, socketListening;
        Runnable client;
        
        socketListening = new ServerComm(LOUNGE_PORT);
        socketListening.start();
        
        System.out.println("[START] Lounge Server starting");
        
        boolean done = false;
        
        /* process requests until END message type is received and processed by the server */
        while (!done) {
            try{
                socketCommunication = socketListening.accept();
                client = new LoungeProxy(socketCommunication, lg);
                Thread c = new Thread(client);
                /* handle exception thrown by proxy thread upon END message processing */
                Thread.UncaughtExceptionHandler h = (Thread th, Throwable ex) -> {
                    System.out.println("Lounge server ended!");
                    System.exit(0);
                };
                
                c.setUncaughtExceptionHandler(h);
                c.start(); 
                
            }
            catch(Exception ex){
                if(lg.getDone()){ // Check if exception is because of end or real exception
                    done = true;
                }else{
                    System.out.print("[EXCEPTION]" + ex);
                }
            }
        } 
    }
    
}
