/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MechanicMain;

import MechanicMain.CommunicationChannel.ClientComm;
import static MechanicMain.CommunicationChannel.CommPorts.*;
/**
 *
 * @author bmend
 */
public class Mechanic implements Runnable{
    private int threadID;
    private MechanicMethods mm = new MechanicMethods();
    private ClientComm gr ;
    private ClientComm lg;
    private ClientComm ra;
    private ClientComm pk;
    int nparts = 3;
    int[] car = new int[1+nparts];
    int[] part_toRepair = new int[nparts];
  
  

  public Mechanic(int threadID){
    this.threadID = threadID;
    this.gr = new ClientComm(GENERAL_REPOSITORY_NAME,GENERAL_REPOSITORY_PORT);
    this.lg = new ClientComm(LOUNGE_NAME, LOUNGE_PORT);
    this.ra = new ClientComm(REPAIR_AREA_NAME, REPAIR_AREA_PORT);
    this.pk = new ClientComm(PARK_NAME, PARK_PORT);
  }

  @Override
  public void run(){
    while(mm.done(threadID)){
       mm.readThePaper(threadID);
       this.car = mm.startRepairProcedure(threadID);
       if(this.car[0] == -1){
           continue;
       }
       //gr.setMechanicState("FTC", threadID);
       mm.getVehicle(this.car[0],threadID);
       int[] order = mm.partAvailable(this.car, threadID);
       //gr.setMechanicState("CST", threadID);
       if(order[0] == -1){
         part_toRepair = mm.getRequiredPart(this.car,this.threadID);
         mm.resumeRepairProcedure(threadID);
         //gr.setMechanicState("FTC", threadID);
         this.car = mm.fixIt(this.car, this.part_toRepair,this.threadID);
         mm.returnVehicle(this.car[0],threadID);
         mm.repairConcluded(this.car[0], threadID);
         //gr.setMechanicState("ALM", threadID);
       }
       else{
         mm.putCarWaiting(this.car,this.threadID);
         mm.letManagerKnow(order, threadID); 
         //gr.setMechanicState("ALM", threadID);
       }
       //gr.setMechanicState("WFW", threadID);
    }
    mm.wakeUpPal(threadID);
  } 
}
