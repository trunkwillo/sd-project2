/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MechanicMain.Monitors;

import MechanicMain.Interfaces.ManagerSupplierSiteInterface;
import MechanicMain.Monitors.GeneralRepository;

import genclass.GenericIO;
import java.util.*;


public class SupplierSite implements ManagerSupplierSiteInterface {
    GeneralRepository gr;
    
    public SupplierSite(GeneralRepository gr){
        this.gr = gr;
    }
    
    @Override
    public synchronized boolean goToSupplierSite(int[] order){
        gr.setManagerState("GNP");
        gr.setOrdersFilled(order);
        return true;
    }
}
