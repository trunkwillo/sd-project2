/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MechanicMain.Monitors;


import MechanicMain.Interfaces.CustomerParkInterface;
import MechanicMain.Interfaces.MechanicParkInterface;
import genclass.GenericIO;
import java.util.*;


public class Park implements CustomerParkInterface, MechanicParkInterface {
  private int[] parking_lot;
  private int rentals;
  private int customer_number;
  
  // Monitors
  GeneralRepository gr;

  public Park(int customer_number, int rentals, GeneralRepository gr){
    this.rentals = rentals;
    this.parking_lot = new int[customer_number + rentals];
    this.customer_number = customer_number;
    this.gr = gr;

    // Creating rental cars
    for (int i = customer_number; i < parking_lot.length; i++){
      parking_lot[i] = 1;
    }
  }
  
  
   // --------------------------------------------- Customer methods ---------------------------------------------
  
    /*
        [Customer] Customer leaves the car in the parking lot.
    */
  
    /**
    * <p> 
    *   Customer enters the parking lot of the repair shop with the car he is currently driving.
    *   It also checks if the spot the customer is parking in is already occupied.
    * </p>
    * @param car is the ID of the car
    * @return Boolean whether it was sucessful to park the car or not.
    */
    @Override
    public synchronized boolean goToRepairShop(int car, int threadID){
        gr.setCustomerState("PRK", threadID);
        if(parking_lot[car] == 0){
            if(car >= 0 && car < 30){
                gr.increaseCustomerParked();
            }else if(car < 33){
                gr.increaseReplacementParked();
            }
            parking_lot[car] = 1;
            return true;
        }
        else return false;
        
    }
    
    /**
    * <p> 
    *   Customer looks for a specific car (replacement) in the parking lot and retrieves it.
    * </p>
    * @param car is the ID of the car
    * @return Boolean whether it was sucessful to retrieve the car from the park or not.
    */
    @Override
    public synchronized boolean findCar(int car, int threadID) {
        gr.setCustomerState("PRK", threadID);
        if(parking_lot[car] == 1){
            parking_lot[car] = 0;
            gr.decreaseReplacementParked();
            return true;
        }
        else return false;
    }
    
    /**
    * <p> 
    *   Customer looks for his car (same Car ID has it's Customer ID) in the parking lot.
    * </p>
    * @param threadID to identify him and his car (they are the same)
    * @return an Integer for further verification if the car was correctly retrieved.
    */
    @Override
    public synchronized int collectCar(int threadID){
        gr.setCustomerCar(String.valueOf(threadID), threadID);
        gr.setCustomerState("PRK", threadID);
        if(parking_lot[threadID] == 1){
            parking_lot[threadID] = 0;
            gr.decreaseCustomerParked();
            return threadID;
        }
        else return 0;
    }
    
    // --------------------------------------------- Mechanic methods ---------------------------------------------
    
    /**
    * <p> 
    *   Mechanic comes to pick up the car from the parking lot for repairs.
    *   It's verified if the car is currently present in the parking lot space reserved for him.</p>
    * <p>
    *   Leaves the parking lot space value at '-1' identifying the car is currently in the repair area for further repairing.
    * </p>
    * @param car Integer with the car ID.
    * @return Boolean if the retrieval was successful.
    */
    @Override
    public synchronized boolean getVehicle(int car){
        if(parking_lot[car] == 1){
            parking_lot[car] = -1;
            return true;
        }
        return false;
    }
    
    /**
    * <p> 
    *   Mechanic comes to the park to drop the repaired car in it's parking lot space.
    *   It's verified whether the parking lot space is already filled.
    * </p>
    * @param car Integer with the car ID.
    * @return Boolean if the delivery was successful.
    */
    @Override
    public synchronized boolean returnVehicle(int car){
        if(parking_lot[car] == -1){
            parking_lot[car] = 1;
            return true;
        }
        return false;
    }
}
