/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MechanicMain.Entities;

import genclass.GenericIO;
import MechanicMain.Monitors.GeneralRepository;
import MechanicMain.Monitors.Lounge;
import MechanicMain.Monitors.OutsideWorld;
import MechanicMain.Monitors.Park;
import java.util.*;

/**
 *
 * @author bmend
 */
public class Customer implements Runnable{
    private int threadID;
    private boolean wantRepCar;
    private boolean hasRepCar;
    private int c;
    private GeneralRepository gr;
    private Lounge lg;
    private OutsideWorld ow;
    private Park pk;
    private int rep_car= -1;
    private int c_temp;

    public Customer(int threadID, GeneralRepository gr, Lounge lg, OutsideWorld ow, Park pk ){
        this.threadID = threadID;
        this.gr = gr;
        this.lg = lg;
        this.ow = ow;
        this.pk = pk;
        this.c = threadID;
        this.wantRepCar = repCar();
    }

    //stats = NORMAL_LIFE_WITH_CAR // NORMAL_LIFE_WITHOUT_CAR // WAITING_FOR_REPLACE_CAR // RECEPTION // PARK

    @Override
    public void run(){
        if(wantRepCar) gr.setReplacementNeeded("T", threadID);
        ow.decideOnRepair(); // wait
        pk.goToRepairShop(threadID, threadID);
        //gr.setCustomerState("PRK", threadID);
        lg.queueIn(threadID);//wait
        //gr.setCustomerState("RCP", threadID);
        lg.talkWithManager(threadID); 
          if(this.wantRepCar){
            rep_car = lg.collectKey(threadID);
            //gr.setCustomerCar(String.valueOf(rep_car), threadID);
            //gr.setCustomerState("WRC", threadID);
            pk.findCar(rep_car, threadID);
            //gr.setCustomerState("PRK", threadID);
            ow.backtoWorkByCar(threadID);
            //gr.setCustomerState("NLC", threadID);
            pk.goToRepairShop(rep_car, threadID);
            //gr.setCustomerState("PRK", threadID);
        }else{
            ow.backtoWorkByBus(threadID);
            //gr.setCustomerCar("-", threadID);
            //gr.setCustomerState("NLO", threadID);
        }
        //gr.setRepairDone("T", threadID);
        lg.queueIn(threadID);
        //gr.setCustomerState("RCP", threadID);
        lg.payForTheService(rep_car);
        c_temp = pk.collectCar(threadID);
        //gr.setCustomerCar(String.valueOf(threadID), threadID);
        //gr.setCustomerState("PRK", threadID);
        if (c != c_temp )  {
            throw new RuntimeException("Returned Wrong Car!");
        }
        ow.backtoWorkByCar(threadID);
        //gr.setCustomerState("NLC", threadID);
    }


// ------------------------------------------------------------
// ----------------- AUX FUNCS --------------------------------

    /**
    * <p> Calculate  the probability of one Costumer ask for 
    * a replace car. </p> 
    *@return True or False to have or not replacement car
    */
    public boolean repCar(){
        return probability();
    }

    /**
    * <p> If are substituion cars to give to the client, this flag is put at True.</p> 
    */
    public void set_hasRepCar(){
        this.hasRepCar = true;
    }



    /**
    * <p> Used to verigy if Costumer has replacement car or not.</p> 
    */
    public boolean get_hasRepCar(){
        return this.hasRepCar;
    }

    /**
    * <p> Small fuction used to calculate the probability of the client goes to talk with the manageror not.</p> 
    *@return True if the probability is higher them 80%, then the Customer goes to talk with the manager.
    */
    private boolean probability(){
        Random gerador = new Random();
        Double prob = 0.0;
        prob = gerador.nextDouble();
        if(prob > 0.8){
            return true;
        }
        else{
            return false;
        }
    }
}
