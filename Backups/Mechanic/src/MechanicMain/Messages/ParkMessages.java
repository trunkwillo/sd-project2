/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package MechanicMain.Messages;

import java.io.Serializable;

/**
 *
 * @author diniscanastro
 */
 public class ParkMessages implements Serializable{

     private int msgType = -1;
     private boolean msgState = false;

     public static final int GO_TO_REPAIR_SHOP = 1;
     public static final int FIND_CAR = 2;
     public static final int COLLECT_CAR = 3;
     public static final int GET_VEHICLES = 4;
     public static final int RETURN_VEHICLES = 5;

     /**
      * Message to signal the client that his request has been processed with success
      */
     public static final int OK = 20;

     /**
      * Message to terminate server activity
      */
     public static final int END = 0;

     private int threadID;
     private int car;

     // Responses
     private boolean response_bool;
     private int response_int;


     /**
      * Constructor with the message type
      * @param msgType type of message received
      * @param threadID
      * @param car
      */
     public ParkMessages(int msgType,int threadID, int car) {
         this.msgType = msgType;
         this.threadID = threadID;
         this.car = car;
     }

     /**
      * Constructor with the message type
      * @param msgType type of message received
      * @param var
      */
     public ParkMessages(int msgType, int var) {
         this.msgType = msgType;
         this.threadID = threadID;
         if(msgType == COLLECT_CAR){
             this.threadID = threadID;
         }else{
             this.car = car;
         }
     }

     /**
      * Constructor with the message type
      * @param msgType type of message received
      * @param var
      */
     public ParkMessages(int msgType) {
         this.msgType = msgType;
     }

     /***
      *
      * @param msgType
      * @param msgState
      * Response
      */
     public ParkMessages(int msgType, boolean msgState) {
         this.msgType = msgType;
         this.msgState = msgState;
     }

     /***
      *
      * @param msgType
      * @param msgState
      * Response
      * @param response
      */
     public ParkMessages(int msgType, boolean msgState, boolean response) {
         this.msgType = msgType;
         this.msgState = msgState;
         this.response_bool = response;
     }

     /***
      *
      * @param msgType
      * @param msgState
      * Response
      */
     public ParkMessages(int msgType, boolean msgState, int response) {
         this.msgType = msgType;
         this.msgState = msgState;
         this.response_int = response;
     }


      /**
      * Getter method returning the message type
      * @return integer value representing the message type
      */
     public int getMessageType(){
         return this.msgType;
     }

     /**
      * Getter method returning the state of the message
      * @return boolean value signaling the state of the message
      */
     public boolean getMessageState(){
         return this.msgState;
     }

     public int getMessageThreadID(){
         return this.threadID;
     }

     public int getMessageCar(){
         return this.car;
     }


     // Responses
     public boolean getMessageResponseBool(){
         return this.response_bool;
     }

     public int getMessageResponseInt(){
         return this.response_int;
     }

 }
