/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GeneralRepository;

import static GeneralRepository.CommunicationChannel.CommPorts.GENERAL_REPOSITORY_PORT;
import GeneralRepository.CommunicationChannel.ServerComm;

/**
 *
 * @author bmend
 */
public class GeneralRepositoryMain  {
    public static void main(String[] args) {
        GeneralRepository gr = new GeneralRepository();
        ServerComm socketCommunication, socketListening;
        Runnable client;
        
        socketListening = new ServerComm(GENERAL_REPOSITORY_PORT);
        socketListening.start();
        
        System.out.println("[START] GENERAL REPOSITORY Server starting");
        
        boolean done = false;
        
        /* process requests until END message type is received and processed by the server */
        while (!done) {
            try{
                socketCommunication = socketListening.accept();
                client = new GeneralRepositoryProxy(socketCommunication, gr);
                Thread c = new Thread(client);
                /* handle exception thrown by proxy thread upon END message processing */
                Thread.UncaughtExceptionHandler h = (Thread th, Throwable ex) -> {
                    System.out.println("Betting center server ended!");
                    System.exit(0);
                };
                
                c.setUncaughtExceptionHandler(h);
                c.start(); 
                
            }
            catch(Exception ex){
                if(gr.getDone()){ // Check if exception is because of end or real exception
                    done = true;
                }else{
                    System.out.print(ex);
                }
            }
        }
        
        System.out.println("Betting center server ended");
        
    }
    
}

