/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package GeneralRepository.Messages;

import java.io.Serializable;
/**
 *
 * @author diniscanastro
 */
public class SupplierSiteMessages implements Serializable {
    
    private int msgType = -1;
    private boolean msgState = false;
    
    public static final int GO_TO_SUPPLIER_SITE = 1;
    
    /**
     * Message to signal the client that his request has been processed with success
     */
    public static final int OK = 20;
    
    /**
     * Message to terminate server activity
     */
    public static final int END = 0;
    
    private int[] order;
    private boolean response;
    
    /**
     * Constructor with the message type
     * @param msgType type of message received
     * @param order order new parts
     */
    public SupplierSiteMessages(int msgType, int[] order) {
        this.msgType = msgType;
        this.order = order;
    }
    
    public SupplierSiteMessages(int msgType, boolean response) {
        this.msgType = msgType;
        this.response = response;
    }
     
     public SupplierSiteMessages(int msgType){
         this.msgType = msgType;
     }
    
     public int getMessageType(){
        return this.msgType;
    }
    
    
    public int[] getMessageOrder(){
        return this.order;
    }
    
    public boolean getMessageResponse(){
        return this.response;
    }
}