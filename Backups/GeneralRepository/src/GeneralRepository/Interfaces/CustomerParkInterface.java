/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package GeneralRepository.Interfaces;

/**
 *
 * @author diniscanastro
 */
public interface CustomerParkInterface {

    /**
     * <p>
     *   Customer looks for his car (same Car ID has it's Customer ID) in the parking lot.
     * </p>
     * @param threadID to identify him and his car (they are the same)
     * @return an Integer for further verification if the car was correctly retrieved.
     */
    int collectCar(int threadID);

    /**
     * <p>
     *   Customer looks for a specific car (replacement) in the parking lot and retrieves it.
     * </p>
     * @param car is the ID of the car
     * @return Boolean whether it was sucessful to retrieve the car from the park or not.
     */
    boolean findCar(int car, int threadID);

    // --------------------------------------------- Customer methods ---------------------------------------------
    /*
    [Customer] Customer leaves the car in the parking lot.
     */
    /**
     * <p>
     *   Customer enters the parking lot of the repair shop with the car he is currently driving.
     *   It also checks if the spot the customer is parking in is already occupied.
     * </p>
     * @param car is the ID of the car
     * @return Boolean whether it was sucessful to park the car or not.
     */
    boolean goToRepairShop(int car, int threadID);
    
}
