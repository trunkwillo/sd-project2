/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ParkMain;

import ParkMain.CommunicationChannel.ServerComm;
import ParkMain.Messages.ParkMessages;

/**
 *
 * @author diniscanastro
 */
public class ParkProxy implements Runnable{
    // Communication
    ServerComm socket;
    Park pk;
    
    public ParkProxy(ServerComm socket, Park pk) {
        this.socket = socket;
        this.pk = pk;
    }
    
    
    @Override
    public void run(){
        
        ParkMessages msg_received, msg_sent = null;
        
        msg_received = (ParkMessages) socket.readObject();
        
        boolean response_bool;
        int response_int;
        
        // Processing the message method
        switch (msg_received.getMessageType()) {
            case ParkMessages.GO_TO_REPAIR_SHOP:
                response_bool = pk.goToRepairShop(msg_received.getMessageCar(), msg_received.getMessageThreadID());
                msg_sent = new ParkMessages(ParkMessages.OK, response_bool);
                break;
                
            case ParkMessages.FIND_CAR:
                response_bool = pk.findCar(msg_received.getMessageCar(), msg_received.getMessageThreadID());
                msg_sent = new ParkMessages(ParkMessages.OK, response_bool);
                break;
                
            case ParkMessages.COLLECT_CAR:
                response_int = pk.collectCar(msg_received.getMessageThreadID());
                msg_sent = new ParkMessages(ParkMessages.OK, response_int);
                break;
                
            case ParkMessages.GET_VEHICLES:
                response_bool = pk.getVehicle(msg_received.getMessageCar());
                msg_sent = new ParkMessages(ParkMessages.OK, response_bool);
                break;
                
            case ParkMessages.RETURN_VEHICLES:
                response_bool = pk.returnVehicle(msg_received.getMessageCar());
                msg_sent = new ParkMessages(ParkMessages.OK, response_bool);
                break;
                

            case ParkMessages.END:
                System.out.println("END");
                pk.setDone();
                msg_sent = new ParkMessages(ParkMessages.OK);
                throw new RuntimeException("Received end message.");
                
            default:
                System.out.println("[ERROR] Received invalid message type: " + msg_received);
                break;
        }
    }
}
