/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ParkMain;

import static ParkMain.CommunicationChannel.CommPorts.PARK_PORT;
import ParkMain.CommunicationChannel.ServerComm;

/**
 *
 * @author bmend
 */
public class ParkMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Park pk = new Park(30, 3);
        ServerComm socketCommunication, socketListening;
        Runnable client;
        
        socketListening = new ServerComm(PARK_PORT);
        socketListening.start();
        
        System.out.println("[START] Park Server starting");
        
        boolean done = false;
        
        /* process requests until END message type is received and processed by the server */
        while (!done) {
            try{
                socketCommunication = socketListening.accept();
                client = new ParkProxy(socketCommunication, pk);
                Thread c = new Thread(client);
                /* handle exception thrown by proxy thread upon END message processing */
                Thread.UncaughtExceptionHandler h = (Thread th, Throwable ex) -> {
                    System.out.println("Park server ended!");
                    System.exit(0);
                };
                
                c.setUncaughtExceptionHandler(h);
                c.start(); 
                
            }
            catch(Exception ex){
                if(pk.getDone()){ // Check if exception is because of end or real exception
                    done = true;
                }else{
                    System.out.print("[EXCEPTION]" + ex);
                }
            }
        } 
        
    }
    
}
