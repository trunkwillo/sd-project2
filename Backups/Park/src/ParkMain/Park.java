/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ParkMain;

import ParkMain.CommunicationChannel.ClientComm;
import static ParkMain.CommunicationChannel.CommPorts.GENERAL_REPOSITORY_NAME;
import static ParkMain.CommunicationChannel.CommPorts.GENERAL_REPOSITORY_PORT;
import ParkMain.Interfaces.CustomerParkInterface;
import ParkMain.Interfaces.MechanicParkInterface;
import ParkMain.Messages.GeneralRepositoryMessages;

/**
 *
 * @author diniscanastro
 */
public class Park implements CustomerParkInterface, MechanicParkInterface{
    private int[] parking_lot;
  private int rentals;
  private int customer_number;
  
  // Monitors
  ClientComm gr;
  
  private boolean done = false;

  public Park(int customer_number, int rentals){
    this.rentals = rentals;
    this.parking_lot = new int[customer_number + rentals];
    this.customer_number = customer_number;
    this.gr = new ClientComm(GENERAL_REPOSITORY_NAME, GENERAL_REPOSITORY_PORT);

    // Creating rental cars
    for (int i = customer_number; i < parking_lot.length; i++){
      parking_lot[i] = 1;
    }
  }
  
  public synchronized void setDone(){
      this.done = true;
  }
  
  public synchronized boolean getDone(){
      return this.done;
  }
  
  
   // --------------------------------------------- Customer methods ---------------------------------------------
  
    /*
        [Customer] Customer leaves the car in the parking lot.
    */
  
    /**
    * <p> 
    *   Customer enters the parking lot of the repair shop with the car he is currently driving.
    *   It also checks if the spot the customer is parking in is already occupied.
    * </p>
    * @param car is the ID of the car
    * @return Boolean whether it was sucessful to park the car or not.
    */
    @Override
    public synchronized boolean goToRepairShop(int car, int threadID){
        setCustomerState("PRK", threadID);
        if(parking_lot[car] == 0){
            if(car >= 0 && car < 30){
                increaseCustomerParked();
            }else if(car < 33){
                increaseReplacementParked();
            }
            parking_lot[car] = 1;
            return true;
        }
        else return false;
        
    }
    
    /**
    * <p> 
    *   Customer looks for a specific car (replacement) in the parking lot and retrieves it.
    * </p>
    * @param car is the ID of the car
    * @return Boolean whether it was sucessful to retrieve the car from the park or not.
    */
    @Override
    public synchronized boolean findCar(int car, int threadID) {
        setCustomerState("PRK", threadID);
        if(parking_lot[car] == 1){
            parking_lot[car] = 0;
            decreaseReplacementParked();
            return true;
        }
        else return false;
    }
    
    /**
    * <p> 
    *   Customer looks for his car (same Car ID has it's Customer ID) in the parking lot.
    * </p>
    * @param threadID to identify him and his car (they are the same)
    * @return an Integer for further verification if the car was correctly retrieved.
    */
    @Override
    public synchronized int collectCar(int threadID){
        setCustomerCar(String.valueOf(threadID), threadID);
        setCustomerState("PRK", threadID);
        if(parking_lot[threadID] == 1){
            parking_lot[threadID] = 0;
            decreaseCustomerParked();
            return threadID;
        }
        else return 0;
    }
    
    // --------------------------------------------- Mechanic methods ---------------------------------------------
    
    /**
    * <p> 
    *   Mechanic comes to pick up the car from the parking lot for repairs.
    *   It's verified if the car is currently present in the parking lot space reserved for him.</p>
    * <p>
    *   Leaves the parking lot space value at '-1' identifying the car is currently in the repair area for further repairing.
    * </p>
    * @param car Integer with the car ID.
    * @return Boolean if the retrieval was successful.
    */
    @Override
    public synchronized boolean getVehicle(int car){
        if(parking_lot[car] == 1){
            parking_lot[car] = -1;
            return true;
        }
        return false;
    }
    
    /**
    * <p> 
    *   Mechanic comes to the park to drop the repaired car in it's parking lot space.
    *   It's verified whether the parking lot space is already filled.
    * </p>
    * @param car Integer with the car ID.
    * @return Boolean if the delivery was successful.
    */
    @Override
    public synchronized boolean returnVehicle(int car){
        if(parking_lot[car] == -1){
            parking_lot[car] = 1;
            return true;
        }
        return false;
    }
    
    // Repository methods
    
    public synchronized void setCustomerState(String s, int threadID){
        GeneralRepositoryMessages response;
        openComm(gr,"Park General Repository Update method.");
        gr.writeObject(new GeneralRepositoryMessages(GeneralRepositoryMessages.SET_CUSTOMER_STATE, s, threadID));
        response = (GeneralRepositoryMessages) gr.readObject(); 
        gr.close();
    }
    
    public synchronized void increaseCustomerParked(){
        GeneralRepositoryMessages response;
        openComm(gr,"Park General Repository Update method.");
        gr.writeObject(new GeneralRepositoryMessages(GeneralRepositoryMessages.INCREASE_CUSTOMER_PARKED));
        response = (GeneralRepositoryMessages) gr.readObject(); 
        gr.close();
    }
    
    public synchronized void increaseReplacementParked(){
        GeneralRepositoryMessages response;
        openComm(gr,"Park General Repository Update method.");
        gr.writeObject(new GeneralRepositoryMessages(GeneralRepositoryMessages.INCREASE_REPLACEMENT_PARKED));
        response = (GeneralRepositoryMessages) gr.readObject(); 
        gr.close();
    }
    
    public synchronized void decreaseCustomerParked(){
        GeneralRepositoryMessages response;
        openComm(gr,"Park General Repository Update method.");
        gr.writeObject(new GeneralRepositoryMessages(GeneralRepositoryMessages.DECREASE_CUSTOMER_PARKED));
        response = (GeneralRepositoryMessages) gr.readObject(); 
        gr.close();
    }
    
    public synchronized void decreaseReplacementParked(){
        GeneralRepositoryMessages response;
        openComm(gr,"Park General Repository Update method.");
        gr.writeObject(new GeneralRepositoryMessages(GeneralRepositoryMessages.DECREASE_REPLACEMENT_PARKED));
        response = (GeneralRepositoryMessages) gr.readObject(); 
        gr.close();
    }
    
    public synchronized void setCustomerCar(String t, int threadID){
        GeneralRepositoryMessages response;
        openComm(gr,"Park General Repository Update method.");
        gr.writeObject(new GeneralRepositoryMessages(GeneralRepositoryMessages.SET_CUSTOMER_CAR, t, threadID));
        response = (GeneralRepositoryMessages) gr.readObject(); 
        gr.close();
    }
    
    private void openComm(ClientComm cc, String name){
        while(!cc.open()){
            System.out.println(name + " not open, trying again...");
            try{
                Thread.sleep(1000);
            }catch(Exception ex){ }            
        }
    }
}
