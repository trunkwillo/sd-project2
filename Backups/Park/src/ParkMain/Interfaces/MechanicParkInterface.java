/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ParkMain.Interfaces;

/**
 *
 * @author diniscanastro
 */
public interface MechanicParkInterface {

    // --------------------------------------------- Mechanic methods ---------------------------------------------
    /**
     * <p>
     *   Mechanic comes to pick up the car from the parking lot for repairs.
     *   It's verified if the car is currently present in the parking lot space reserved for him.</p>
     * <p>
     *   Leaves the parking lot space value at '-1' identifying the car is currently in the repair area for further repairing.
     * </p>
     * @param car Integer with the car ID.
     * @return Boolean if the retrieval was successful.
     */
    boolean getVehicle(int car);

    /**
     * <p>
     *   Mechanic comes to the park to drop the repaired car in it's parking lot space.
     *   It's verified whether the parking lot space is already filled.
     * </p>
     * @param car Integer with the car ID.
     * @return Boolean if the delivery was successful.
     */
    boolean returnVehicle(int car);
    
}
