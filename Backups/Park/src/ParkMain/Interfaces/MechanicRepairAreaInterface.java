/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ParkMain.Interfaces;

/**
 *
 * @author diniscanastro
 */
public interface MechanicRepairAreaInterface extends ManagerRepairAreaInterface {

    /**
     * <p> Used to verify if already repair all the cars and if any car is in the suspend list,
     * and if all order of works are done. </p>
     *@return  number of cars that were rapaired,
     */
    boolean done();

    /**
     * <p> Mechanic fix the car  . </p>
     *@param car Car is an array, on what the first index is the id of the car, and the rest is the parts needed to repair this car.
     *@param part_toRepair array with parts needed to rapir the car, after take of the store.
     *@return An array, on what the first index is the id of the car, and the rest is the parts fix of the car.
     */
    int[] fixIt(int[] car, int[] part_toRepair);

    /**
     * <p> Used to count the number of car repaired. </p>
     *@return  number of cars that were rapaired,
     */
    int getCarCount();

    /**
     * <p> If there is enougth Parts to repair the car, the Mechanic take that parts from the stock. </p>
     * @param car Car is an array, on what the first index is the id of the car, and the rest is the parts needed to repair this car.
     * @return Is returned an array with the number of parts that need to be repair the car.
     */
    int[] getRequiredPart(int[] car);

    /**
     * <p> If there is enougth Parts to repair the car, send information saying that,
     * if not then send an order to Manager asking for more . </p>
     * @param car Car is an array, on what the first index is the id of the car, and the rest is the parts needed to repair this car.
     * @return Is returned an array with the number of parts,in lack, that need to be repair the car.
     */
    int[] partAvailable(int[] car, int threadID);

    /**
     * <p> Mechanic put the car in suspense when he has no parts to repair this car . </p>
     *@param car Car is an array, on what the first index is the id of the car, and the rest is the parts needed to repair this car.
     */
    void putCarWaiting(int[] car);

    /**
     * <p> Mechanic in this function stay waiting for something to do,
     *  he is waiting to be waked by the Manager. </p>
     */
    void readThePaper(int threadID);

    /**
     * <p>Used to change the state of the mechanic. </p>
     */
    void resumeRepairProcedure(int threadID);

    /**
     * <p> Mechanic now have a car to repair so he start the procedure to repair the car. </p>
     *@return Is returned an array with the car the we start to repair.
     */
    int[] startRepairProcedure(int threadID);

    // ------------------------------------------------------------
    // ----------------- AUX FUNCS --------------------------------
    /**
     * <p> This function is to notify the two mechanics to say that the day is over, and can call out the day. </p>
     */
    void wakeUpPal();
    
}
