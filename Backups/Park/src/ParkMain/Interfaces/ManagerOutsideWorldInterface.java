/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ParkMain.Interfaces;

/**
 *
 * @author diniscanastro
 */
public interface ManagerOutsideWorldInterface {

    /**
     * <p> Car is ready to go, and Manager call to Customer to alerting him. </p>
     *
     * @param car_owner Id of ther car owner to notify him.
     */
    void phoneCustomer(int car_owner);
    
}
